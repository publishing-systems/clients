/* Copyright (C) 2016-2021  Stephan Kreutzer
 *
 * This file is part of wakelet_downloader_1.
 *
 * wakelet_downloader_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * wakelet_downloader_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with wakelet_downloader_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2019-07-24
 */



import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.ArrayList;



public class wakelet_downloader_1
{
    public static void main(String[] args)
    {
        System.out.print("wakelet_downloader_1 Copyright (C) 2016-2021 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/clients/ and the project website\n" +
                         "https://www.publishing-systems.org.\n\n");

        wakelet_downloader_1 instance = new wakelet_downloader_1();
        instance.run(args);
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("Usage:\n\twakelet_downloader_1 wakelet-wake-url target-directory\n");
            System.exit(1);
        }

        this.programPath = wakelet_downloader_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            this.programPath = new File(this.programPath).getCanonicalPath() + File.separator;
            this.programPath = URLDecoder.decode(this.programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("wakelet_downloader_1: Can't determine program path.");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: Can't determine program path.");
            System.exit(1);
        }

        return attemptDownload(args[0], args[1], this.programPath);
    }

    protected int attemptDownload(String wakeUrl, String outputParentPath, String tempParentPath)
    {
        return attemptDownload(wakeUrl, outputParentPath, tempParentPath, -1);
    }

    protected int attemptDownload(String wakeUrl, String outputParentPath, String tempParentPath, int index)
    {
        String apiUrl = "https://api.wakelet.com/collections/";

        {
            String[] parts = wakeUrl.split("/");

            if (parts.length <= 0)
            {
                System.out.println("wakelet_downloader_1: Parameter argument \"" + wakeUrl + "\" for the Wakelet wake URL doesn't contain the wake ID part.");
                System.exit(1);
            }

            String id = parts[parts.length - 1];

            if (this.globalWakeIds.contains(id) == true)
            {
                System.out.println("wakelet_downloader_1: Skipping wake with ID '" + id + "', already downloaded.");
                return 0;
            }

            this.globalWakeIds.add(id);
            System.out.println("wakelet_downloader_1: Downloading wake with ID '" + id + "'.");

            apiUrl += parts[parts.length - 1];
            apiUrl += "/cards";
        }

        if (tempParentPath.endsWith("/") != true &&
            tempParentPath.endsWith("\\") != true)
        {
            tempParentPath += File.separator;
        }

        File tempDirectory = null;

        if (index >= 0)
        {
            tempDirectory = new File(tempParentPath + "temp-" + index);
        }
        else
        {
            tempDirectory = new File(tempParentPath + "temp");
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("wakelet_downloader_1: Can't create temp directory \"" + tempDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        if (outputParentPath.endsWith("/") != true &&
            outputParentPath.endsWith("\\") != true)
        {
            outputParentPath += File.separator;
        }

        File outputDirectory = null;

        if (index >= 0)
        {
            outputDirectory = new File(outputParentPath + "wake-" + index);
        }
        else
        {
            outputDirectory = new File(outputParentPath + "wake");
        }

        try
        {
            outputDirectory = outputDirectory.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("wakelet_downloader_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (outputDirectory.exists() == true)
        {
            if (outputDirectory.isDirectory() == true)
            {
                System.out.println("wakelet_downloader_1: Output directory \"" + outputDirectory.getAbsolutePath() + "\" does already exist.");
                System.exit(1);
            }
            else
            {
                System.out.println("wakelet_downloader_1: Output path \"" + outputDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                outputDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("wakelet_downloader_1: Can't create output directory \"" + outputDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        int pageCount = 0;
        List<String> subWakeUrls = new ArrayList<String>();
        int cardCount = 1;

        for (; apiUrl != null; pageCount++)
        {
            attemptRetrieval(apiUrl, "wake_page_" + pageCount, tempDirectory);
            apiUrl = null;

            File wakeJsonFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resource_wake_page_" + pageCount);
            File wakeJsonXmlFile = new File(tempDirectory.getAbsolutePath() + File.separator + "wake-page-" + pageCount + ".jsonxml");

            jsonToXml(wakeJsonFile, tempDirectory, wakeJsonXmlFile);

            File wakeJsonXmlPreparationStylesheet = new File(this.programPath + "jsonxml_preparation_stylesheet_wake.xsl");
            File wakeXmlFile = new File(tempDirectory.getAbsolutePath() + File.separator + "wake-page-" + pageCount + ".xml");

            xmlTransform(wakeJsonXmlFile, tempDirectory, wakeJsonXmlPreparationStylesheet, wakeXmlFile);


            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(wakeXmlFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("page") == true)
                        {
                            Attribute attributeNext = event.asStartElement().getAttributeByName(new QName("next"));

                            if (attributeNext == null)
                            {
                                continue;
                            }

                            apiUrl = "https://api.wakelet.com" + attributeNext.getValue();
                        }
                        else if (tagName.equals("description") == true)
                        {
                            File cardDestinationFile = new File(outputDirectory.getAbsolutePath() + File.separator + "card-" + cardCount + ".xhtml");

                            if (cardDestinationFile.exists() == true)
                            {
                                if (cardDestinationFile.isFile() == true)
                                {
                                    boolean deleteSuccessful = false;

                                    try
                                    {
                                        deleteSuccessful = cardDestinationFile.delete();
                                    }
                                    catch (SecurityException ex)
                                    {

                                    }

                                    if (deleteSuccessful != true)
                                    {
                                        if (cardDestinationFile.canWrite() != true)
                                        {
                                            System.out.println("wakelet_downloader_1: Can't overwrite output file \"" + cardDestinationFile.getAbsolutePath() + "\".");
                                            System.exit(1);
                                        }
                                    }
                                }
                                else
                                {
                                    System.out.println("wakelet_downloader_1: Output path \"" + cardDestinationFile.getAbsolutePath() + "\" isn't a file.");
                                    System.exit(1);
                                }
                            }

                            try
                            {
                                BufferedWriter writer = new BufferedWriter(
                                                        new OutputStreamWriter(
                                                        new FileOutputStream(cardDestinationFile),
                                                        "UTF-8"));

                                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                                writer.write("<!DOCTYPE html\n");
                                writer.write("    PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n");
                                writer.write("    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
                                writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"de\" lang=\"de\">\n");
                                writer.write("  <head>\n");
                                writer.write("    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n");
                                writer.write("    <!-- This file was created by wakelet_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
                                writer.write("    <title>Wakelet Card</title>\n");
                                writer.write("  </head>\n");
                                writer.write("  <body>\n");
                                writer.write("    <div>\n");

                                while (eventReader.hasNext() == true)
                                {
                                    event = eventReader.nextEvent();

                                    if (event.isCharacters() == true)
                                    {
                                        // This de-escapes the XML special characters from the
                                        // XHTML payload as obtained from JSON.
                                        writer.write(event.asCharacters().getData());
                                    }
                                    else if (event.isEndElement() == true)
                                    {
                                        tagName = event.asEndElement().getName().getLocalPart();

                                        if (tagName.equals("description") == true)
                                        {
                                            break;
                                        }
                                    }
                                }

                                writer.write("    </div>\n");
                                writer.write("  </body>\n");
                                writer.write("</html>\n");

                                writer.flush();
                                writer.close();
                            }
                            catch (FileNotFoundException ex)
                            {
                                System.out.println("wakelet_downloader_1: An error occurred while writing output file \"" + cardDestinationFile.getAbsolutePath() + "\".");
                                System.exit(1);
                            }
                            catch (UnsupportedEncodingException ex)
                            {
                                System.out.println("wakelet_downloader_1: An error occurred while writing output file \"" + cardDestinationFile.getAbsolutePath() + "\".");
                                System.exit(1);
                            }
                            catch (IOException ex)
                            {
                                System.out.println("wakelet_downloader_1: An error occurred while writing output file \"" + cardDestinationFile.getAbsolutePath() + "\".");
                                System.exit(1);
                            }

                            ++cardCount;
                        }
                        else if (tagName.equals("sub-wake") == true)
                        {
                            Attribute attributeUrl = event.asStartElement().getAttributeByName(new QName("url"));

                            if (attributeUrl == null)
                            {
                                continue;
                            }

                            String subWakeUrl = attributeUrl.getValue().split("\\?")[0];

                            if (subWakeUrls.contains(subWakeUrl) != true)
                            {
                                subWakeUrls.add(subWakeUrl);
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading \"" + wakeXmlFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (SecurityException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading \"" + wakeXmlFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading \"" + wakeXmlFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        {
            File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_concatenator_1.xml");
            File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_concatenator_1.xml");
            File concatenatedWakeXmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + "wake.xml");

            if (jobFile.exists() == true)
            {
                if (jobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = jobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (jobFile.canWrite() != true)
                        {
                            System.out.println("wakelet_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                    }
                }
                else
                {
                    System.out.println("wakelet_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                    System.exit(1);
                }
            }

            if (resultInfoFile.exists() == true)
            {
                if (resultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = resultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (resultInfoFile.canWrite() != true)
                        {
                            System.out.println("wakelet_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                    }
                }
                else
                {
                    System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                    System.exit(1);
                }
            }

            if (concatenatedWakeXmlFile.exists() == true)
            {
                if (concatenatedWakeXmlFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = concatenatedWakeXmlFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (concatenatedWakeXmlFile.canWrite() != true)
                        {
                            System.out.println("wakelet_downloader_1: Can't overwrite output file \"" + concatenatedWakeXmlFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                    }
                }
                else
                {
                    System.out.println("wakelet_downloader_1: Output path \"" + concatenatedWakeXmlFile.getAbsolutePath() + "\" isn't a file.");
                    System.exit(1);
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(jobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by wakelet_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
                writer.write("<xml-concatenator-1-job>\n");
                writer.write("  <input-files>\n");

                for (int i = 0; i < pageCount; i++)
                {
                    writer.write("    <input-file path=\"" + tempDirectory.getAbsolutePath() + File.separator + "wake-page-" + i + ".xml\"/>\n");
                }

                writer.write("  </input-files>\n");
                writer.write("  <output-file path=\"" + concatenatedWakeXmlFile.getAbsolutePath() + "\" processing-instruction-data=\"encoding=&quot;UTF-8&quot;\" root-element-name=\"wake\"/>\n");
                writer.write("</xml-concatenator-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (UnsupportedEncodingException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
                System.exit(1);
            }

            ProcessBuilder builder = new ProcessBuilder("java", "xml_concatenator_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
            builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "xml_concatenator" + File.separator + "xml_concatenator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading xml_concatenator_1 output.");
                System.exit(1);
            }

            if (resultInfoFile.exists() != true)
            {
                System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
                System.exit(1);
            }

            if (resultInfoFile.isFile() != true)
            {
                System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
                System.exit(1);
            }

            if (resultInfoFile.canRead() != true)
            {
                System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
                System.exit(1);
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(resultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (SecurityException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                System.exit(1);
            }

            if (wasSuccess != true)
            {
                System.out.println("wakelet_downloader_1: xml_concatenator_1 call wasn't successful.");
                System.exit(1);
            }
        }

        for (int i = 0; i < subWakeUrls.size(); i++)
        {
            int result = attemptDownload(subWakeUrls.get(i), outputDirectory.getAbsolutePath(), tempDirectory.getAbsolutePath(), i + 1);

            if (result != 0)
            {
                return result;
            }
        }

        return 0;
    }

    public int attemptRetrieval(String url, String name, File tempDirectory)
    {
        if (tempDirectory.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (tempDirectory.isDirectory() != true)
        {
            System.out.println("wakelet_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (tempDirectory.canWrite() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        // Ampersand needs to be the first, otherwise it would double-encode
        // other entities.
        url = url.replaceAll("&", "&amp;");
        url = url.replaceAll("<", "&lt;");
        url = url.replaceAll(">", "&gt;");
        url = url.replaceAll("\"", "&quot;");
        url = url.replaceAll("'", "&apos;");

        File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_https_client_1_" + name + ".xml");
        File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_https_client_1_" + name + ".xml");
        File resourceFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resource_" + name);

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resourceFile.exists() == true)
        {
            if (resourceFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resourceFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resourceFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite resource file \"" + resourceFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Resource path \"" + resourceFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wakelet_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
            writer.write("<https-client-1-jobfile>\n");
            writer.write("  <request url=\"" + url + "\" method=\"GET\"/>\n");
            writer.write("  <response destination=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
            writer.write("</https-client-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "https_client_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "https_client" + File.separator + "https_client_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading https_client_1 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("wakelet_downloader_1: https_client_1 call wasn't successful.");
            System.exit(1);
        }

        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
            System.exit(-1);
        }

        return 0;
    }

    public int jsonToXml(File inputFile, File tempDirectory, File outputFile)
    {
        if (inputFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        if (tempDirectory.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (tempDirectory.isDirectory() != true)
        {
            System.out.println("wakelet_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (tempDirectory.canWrite() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_json_to_xml_2.xml");
        File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_json_to_xml_2.xml");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (outputFile.exists() == true)
        {
            if (outputFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = outputFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (outputFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite output file \"" + outputFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Output path \"" + outputFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wakelet_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
            writer.write("<json-to-xml-2-jobfile>\n");
            writer.write("  <json-input-file path=\"" + inputFile.getAbsolutePath() + "\"/>\n");
            writer.write("  <xml-output-file path=\"" + outputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</json-to-xml-2-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "json_to_xml_2", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "json_to_xml" + File.separator + "json_to_xml_2"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading json_to_xml_2 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("wakelet_downloader_1: json_to_xml_2 call wasn't successful.");
            System.exit(1);
        }

        return 0;
    }

    public int xmlTransform(File inputFile, File tempDirectory, File stylesheetFile, File outputFile)
    {
        if (inputFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        if (stylesheetFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Stylesheet file \"" + stylesheetFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (stylesheetFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Stylesheet path \"" + stylesheetFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (stylesheetFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Stylesheet file \"" + stylesheetFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        if (tempDirectory.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (tempDirectory.isDirectory() != true)
        {
            System.out.println("wakelet_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (tempDirectory.canWrite() != true)
        {
            System.out.println("wakelet_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1.xml");
        File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1.xml");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (outputFile.exists() == true)
        {
            if (outputFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = outputFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (outputFile.canWrite() != true)
                    {
                        System.out.println("wakelet_downloader_1: Can't overwrite output file \"" + outputFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("wakelet_downloader_1: Output path \"" + outputFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wakelet_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
            writer.write("<xml-xslt-transformator-1-jobfile>\n");
            writer.write("  <job input-file=\"" + inputFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + this.programPath + "digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_empty.xml\" stylesheet-file=\"" + stylesheetFile.getAbsolutePath() + "\" output-file=\"" + outputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</xml-xslt-transformator-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading xml_xslt_transformator_1 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("wakelet_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("wakelet_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("wakelet_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("wakelet_downloader_1: xml_xslt_transformator_1 call wasn't successful.");
            System.exit(1);
        }

        return 0;
    }

    protected String programPath = null;
    protected List<String> globalWakeIds = new ArrayList<String>();
}
