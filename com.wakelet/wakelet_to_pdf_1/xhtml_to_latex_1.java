/* Copyright (C) 2016-2019  Stephan Kreutzer
 *
 * This file is part of xhtml_to_latex_1.
 *
 * xhtml_to_latex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xhtml_to_latex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_to_latex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2019-08-19
 */



import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.ArrayList;



public class xhtml_to_latex_1
{
    public static void main(String[] args)
    {
        System.out.print("xhtml_to_latex_1 Copyright (C) 2016-2019 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/clients/ and the project website\n" +
                         "http://www.publishing-systems.org.\n\n");

        xhtml_to_latex_1 instance = new xhtml_to_latex_1();
        instance.run(args);
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("Usage:\n\txhtml_to_latex_1 input-directory output-directory\n");
            System.exit(1);
        }

        String programPath = xhtml_to_latex_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't determine program path.");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't determine program path.");
            System.exit(1);
        }

        File inputDirectory = new File(args[0]);

        try
        {
            inputDirectory = inputDirectory.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't get canonical path of input directory \"" + inputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't get canonical path of input directory \"" + inputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (inputDirectory.exists() != true)
        {
            System.out.println("xhtml_to_latex_1: Input directory \"" + inputDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputDirectory.isDirectory() != true)
        {
            System.out.println("xhtml_to_latex_1: Input path \"" + inputDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (inputDirectory.canWrite() != true)
        {
            System.out.println("xhtml_to_latex_1: Input directory \"" + inputDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }


        File outputDirectory = new File(args[1]);

        try
        {
            outputDirectory = outputDirectory.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (outputDirectory.exists() == true)
        {
            if (outputDirectory.isDirectory() == true)
            {
                if (outputDirectory.canWrite() != true)
                {
                    System.out.println("xhtml_to_latex_1: Output directory \"" + outputDirectory.getAbsolutePath() + "\" isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("xhtml_to_latex_1: Output path \"" + outputDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                outputDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("xhtml_to_latex_1: Can't create output directory \"" + outputDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        File tempDirectory = new File(programPath + "temp" + File.separator + "print");

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    System.out.println("xhtml_to_latex_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("xhtml_to_latex_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("xhtml_to_latex_1: Can't create temp directory \"" + tempDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        File sourceListFile = new File(tempDirectory.getAbsolutePath() + File.separator + "sources.xml");

        if (sourceListFile.exists() == true)
        {
            if (sourceListFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = sourceListFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (sourceListFile.canWrite() != true)
                    {
                        System.out.println("xhtml_to_latex_1: Can't overwrite output file \"" + sourceListFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("xhtml_to_latex_1: Output path \"" + sourceListFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        StringBuilder fileDiscovery1JobfileContentStringBuilder = new StringBuilder();
        fileDiscovery1JobfileContentStringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        fileDiscovery1JobfileContentStringBuilder.append("<file-discovery-1-jobfile>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  <input>\n");
        fileDiscovery1JobfileContentStringBuilder.append("    <directory path=\"" + inputDirectory.getAbsolutePath() + "\"/>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  </input>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  <settings>\n");
        fileDiscovery1JobfileContentStringBuilder.append("    <include-directories set=\"false\"/>\n");
        fileDiscovery1JobfileContentStringBuilder.append("    <include-files set=\"true\"/>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  </settings>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  <output>\n");
        fileDiscovery1JobfileContentStringBuilder.append("    <result-file path=\"" + sourceListFile.getAbsolutePath() + "\"/>\n");
        fileDiscovery1JobfileContentStringBuilder.append("  </output>\n");
        fileDiscovery1JobfileContentStringBuilder.append("</file-discovery-1-jobfile>\n");

        File fileDiscovery1Jobfile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_file_discovery_1.xml");
        File fileDiscovery1Path = new File(programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "file_discovery" + File.separator + "file_discovery_1");
        File fileDiscovery1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_file_discovery_1.xml");
        callCapability(fileDiscovery1Jobfile, fileDiscovery1JobfileContentStringBuilder.toString(), "file_discovery_1", fileDiscovery1Path, fileDiscovery1ResultInfoFile);

        List<File> sourceList = new ArrayList<File>();

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(sourceListFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("entry") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            System.out.println("xhtml_to_latex_1: Element \"" + tagName + "\" is missing it's \"path\" attribute in \"" + sourceListFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }

                        File sourceFile = new File(attributePath.getValue());

                        if (sourceFile.isAbsolute() != true)
                        {
                            sourceFile = new File(sourceListFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            sourceFile = sourceFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            System.out.println("xhtml_to_latex_1: Can't get canonical path of source file path \"" + sourceFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                        catch (IOException ex)
                        {
                            System.out.println("xhtml_to_latex_1: Can't get canonical path of source file path \"" + sourceFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }

                        if (sourceFile.exists() != true)
                        {
                            System.out.println("xhtml_to_latex_1: Source file \"" + sourceFile.getAbsolutePath() + "\" doesn't exist.");
                            System.exit(1);
                        }

                        if (sourceFile.isFile() != true)
                        {
                            System.out.println("xhtml_to_latex_1: Source path \"" + sourceFile.getAbsolutePath() + "\" isn't a file.");
                            System.exit(1);
                        }

                        if (sourceFile.canWrite() != true)
                        {
                            System.out.println("xhtml_to_latex_1: Source file \"" + sourceFile.getAbsolutePath() + "\" isn't writable.");
                            System.exit(1);
                        }

                        /**
                         * @todo Don't rely on non-XML doctype declaration any more, instead,
                         *     use a generic proper XML reader to check for namespace.
                         */

                        String doctypeDeclaration = new String("<!DOCTYPE");
                        int doctypePosMatching = 0;
                        String doctype = new String();

                        try
                        {
                            FileInputStream in2 = new FileInputStream(sourceFile);

                            int currentByte = 0;

                            do
                            {
                                currentByte = in2.read();

                                if (currentByte < 0 ||
                                    currentByte > 255)
                                {
                                    break;
                                }


                                char currentByteCharacter = (char) currentByte;

                                if (doctypePosMatching < doctypeDeclaration.length())
                                {
                                    if (currentByteCharacter == doctypeDeclaration.charAt(doctypePosMatching))
                                    {
                                        doctypePosMatching++;
                                        doctype += currentByteCharacter;
                                    }
                                    else
                                    {
                                        doctypePosMatching = 0;
                                        doctype = new String();
                                    }
                                }
                                else
                                {
                                    doctype += currentByteCharacter;

                                    if (currentByteCharacter == '>')
                                    {
                                        break;
                                    }
                                }

                            } while (true);
                        }
                        catch (FileNotFoundException ex)
                        {
                            System.out.println("xhtml_to_latex_1: An error occurred while reading \"" + sourceFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                        catch (IOException ex)
                        {
                            System.out.println("xhtml_to_latex_1: An error occurred while reading \"" + sourceFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }

                        if (doctype.contains("\"-//W3C//DTD XHTML 1.0 Strict//EN\"") == true)
                        {
                            sourceList.add(sourceFile);
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading \"" + sourceListFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading \"" + sourceListFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading \"" + sourceListFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (CopyFileBinary(new File(programPath + "automated_digital_publishing" + File.separator + "latex" + File.separator + "html_prepare4latex1" + File.separator + "entities" + File.separator + "config_xhtml1-strict.xml"), 
                           new File(programPath + "automated_digital_publishing" + File.separator + "latex" + File.separator + "html_prepare4latex1" + File.separator + "entities" + File.separator + "config.xml")) != 0)
        {
            System.exit(-1);
        }

        List<File> latexFileList = new ArrayList<File>();

        for (int i = 0; i < sourceList.size(); i++)
        {
            File preparedFile = new File(tempDirectory.getAbsolutePath() + File.separator + "xhtml_prepared-" + (i + 1) + ".xhtml");

            ProcessBuilder builder = new ProcessBuilder("java", "html_prepare4latex1", sourceList.get(i).getAbsolutePath(), preparedFile.getAbsolutePath());
            builder.directory(new File(programPath + "automated_digital_publishing" + File.separator + "latex" + File.separator + "html_prepare4latex1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                System.out.println("xhtml_to_latex_1: An error occurred while reading html_prepare4latex1 output.");
                System.exit(1);
            }

            File latexFile = new File(outputDirectory.getAbsolutePath() + File.separator + "printrun-" + (i + 1) + ".tex");

            if (latexFile.exists() == true)
            {
                if (latexFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = latexFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (latexFile.canWrite() != true)
                        {
                            System.out.println("xhtml_to_latex_1: Can't overwrite file \"" + latexFile.getAbsolutePath() + "\".");
                            System.exit(1);
                        }
                    }
                }
                else
                {
                    System.out.println("xhtml_to_latex_1: Path \"" + latexFile.getAbsolutePath() + "\" isn't a file.");
                    System.exit(1);
                }
            }

            latexFileList.add(latexFile);

            /** @todo This would be much smarter if the preparations can be completed in advance,and
              * and then doing the transformations in one go with one jobfile containing several jobs. */
            StringBuilder xmlXsltTransformator1JobfileContentStringBuilder = new StringBuilder();
            xmlXsltTransformator1JobfileContentStringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            xmlXsltTransformator1JobfileContentStringBuilder.append("<xml-xslt-transformator-1-jobfile>\n");
            xmlXsltTransformator1JobfileContentStringBuilder.append("  <job input-file=\"" + preparedFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_xhtml_1_0_strict.xml\" stylesheet-file=\"" + programPath + File.separator + "automated_digital_publishing" + File.separator + "html2latex" + File.separator + "html2latex1" + File.separator + "layout" + File.separator + "xhtml_to_latex_2.xsl\" output-file=\"" + latexFile.getAbsolutePath() + "\"/>\n");
            xmlXsltTransformator1JobfileContentStringBuilder.append("</xml-xslt-transformator-1-jobfile>\n");

            File xmlXsltTransformator1Jobfile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1-" + (i + 1) + ".xml");
            File xmlXsltTransformator1Path = new File(programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1");
            File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1-" + (i + 1) + ".xml");
            callCapability(xmlXsltTransformator1Jobfile, xmlXsltTransformator1JobfileContentStringBuilder.toString(), "xml_xslt_transformator_1", xmlXsltTransformator1Path, xmlXsltTransformator1ResultInfoFile);
        }

        for (int i = 0; i < latexFileList.size(); i++)
        {
            for (int j = 0; j < 4; j++)
            {
                ProcessBuilder builder = new ProcessBuilder("pdflatex", "-interaction=nonstopmode", latexFileList.get(i).getAbsolutePath());
                builder.directory(latexFileList.get(i).getParentFile());
                builder.redirectErrorStream(true);

                try
                {
                    Process process = builder.start();
                    Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                    while (scanner.hasNext() == true)
                    {
                        System.out.println(scanner.next());
                    }

                    scanner.close();
                }
                catch (IOException ex)
                {
                    System.out.println("xhtml_to_latex_1: An error occurred while reading pdflatex output.");
                    System.exit(1);
                }
            }
        }

        return 0;
    }

    public int callCapability(File jobFile, String jobfileContent, String capabilityName, File capabilityPath, File resultInfoFile)
    {
        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("xhtml_to_latex_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("xhtml_to_latex_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("xhtml_to_latex_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("xhtml_to_latex_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write(jobfileContent);
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", capabilityName, jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(capabilityPath);
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading " + capabilityName + " output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("xhtml_to_latex_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("xhtml_to_latex_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("xhtml_to_latex_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("xhtml_to_latex_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("xhtml_to_latex_1: " + capabilityName + " call wasn't successful.");
            /** @todo return -1; */
            System.exit(1);
        }

        return 0;
    }

    public int CopyFileBinary(File from, File to)
    {
        if (from.exists() != true)
        {
            System.out.println("xhtml_to_latex_1: File \"" + from.getAbsolutePath() + "\" doesn't exist.");
            return -1;
        }

        if (from.isFile() != true)
        {
            System.out.println("xhtml_to_latex_1: Path \"" + from.getAbsolutePath() + "\" isn't a file.");
            return -2;
        }

        if (from.canRead() != true)
        {
            System.out.println("xhtml_to_latex_1: File \"" + from.getAbsolutePath() + "\" isn't readable.");
            return -3;
        }

        if (to.exists() == true)
        {
            if (to.isFile() != true)
            {
                System.out.println("xhtml_to_latex_1: Path \"" + to.getAbsolutePath() + "\" isn't a file.");
                return -4;
            }
            else
            {
                if (to.canWrite() != true)
                {
                    System.out.println("xhtml_to_latex_1: File \"" + to.getAbsolutePath() + "\" isn't writable.");
                    return -5;
                }
            }
        }


        boolean exception = false;

        byte[] buffer = new byte[1024];

        FileInputStream reader = null;
        FileOutputStream writer = null;

        try
        {
            to.createNewFile();

            reader = new FileInputStream(from);
            writer = new FileOutputStream(to);

            int bytesRead = reader.read(buffer, 0, buffer.length);

            while (bytesRead > 0)
            {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer, 0, buffer.length);
            }

            writer.close();
            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            exception = true;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            exception = true;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException ex)
                {
                    if (exception == false)
                    {
                        exception = true;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                    }
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ex)
                {
                    if (exception == false)
                    {
                        exception = true;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                    }
                }
            }
        }

        if (exception != false)
        {
            System.exit(-1);
        }

        return 0;
    }
}
