<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2016-2021 Stephan Kreutzer

This file is part of now_client_1 of clients for
automated_digital_publishing/digital_publishing_workflow_tools.

now_client_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

now_client_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with now_client_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8"/>

  <xsl:template match="/rss">
    <xsl:comment> This file was created by rss_to_xml_xslt_transformator_1_jobfile_1.xsl of now_client_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). </xsl:comment>
    <resource-retriever-1-workflow-jobfile>
      <resources>
        <xsl:apply-templates/>
      </resources>
      <output-directory path="./output/"/>
    </resource-retriever-1-workflow-jobfile>
  </xsl:template>

  <xsl:template match="/rss/channel/item/link/text()">
    <resource>
      <xsl:attribute name="identifier">
        <xsl:value-of select="."/>
      </xsl:attribute>
    </resource>
  </xsl:template>

  <xsl:template match="@*|text()"/>

</xsl:stylesheet>
