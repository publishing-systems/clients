#!/bin/sh
# Copyright (C) 2019-2021 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# sudo apt-get install wget unzip make openjdk-8-jdk
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip ./digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
java -cp ./workflows/setup/setup_1/ setup_1
cd ..
