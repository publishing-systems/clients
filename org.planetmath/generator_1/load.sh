#!/bin/sh
# Copyright (C) 2022 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

wget https://github.com/planetmath/11_Number_theory/archive/refs/heads/master.zip
mv master.zip 11_Number_theory.zip
unzip 11_Number_theory.zip
mv 11_Number_theory-master 11_Number_theory
