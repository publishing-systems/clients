/* Copyright (C) 2021-2022  Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JTeroStA.java
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2021-11-11
 */



import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Map;
import org.publishing_systems._20140527t120137z.jterosta.JTeroLoader;
import org.publishing_systems._20140527t120137z.jterosta.JTeroPattern;
import org.publishing_systems._20140527t120137z.jterosta.JTeroFunction;
import org.publishing_systems._20140527t120137z.jterosta.JTeroStAInterpreter;
import org.publishing_systems._20140527t120137z.jterosta.JTeroEvent;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamInterface;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamStd;
import org.publishing_systems._20140527t120137z.jterosta.JTeroException;



public class JTeroStA
{
    public static void main(String args[])
    {
        try
        {
            if (args.length >= 1)
            {
                File inputFile = new File(args[0]);

                if (inputFile.exists() != true)
                {
                    System.out.println("File '" + args[0] + "' doesn't exist.");
                    return;
                }

                if (inputFile.isFile() != true)
                {
                    System.out.println("Path '" + args[0] + "' isn't a file.");
                    return;
                }

                if (inputFile.canRead() != true)
                {
                    System.out.println("File '" + args[0] + "' isn't readable.");
                    return;
                }

                InputStream in = new FileInputStream(inputFile);

                Run(in);
            }
            else
            {
                System.out.print("Usage:\n\n\tJTeroStA <input-html-file>\n\n");
            }
        }
        catch (Exception ex)
        {
            System.out.println("Exception: " + ex.getMessage());
            return;
        }
    }

    public static int Run(InputStream inputStream) throws JTeroException
    {
        JTeroInputStreamInterface stream = new JTeroInputStreamStd(inputStream);

        JTeroLoader teroLoader = new JTeroLoader("./LaTeXPlanetMath/");
        teroLoader.load(true);

        Map<String, JTeroPattern> patterns = teroLoader.getPatterns();
        Map<String, JTeroFunction> functions = teroLoader.getFlow();

        JTeroStAInterpreter interpreter = new JTeroStAInterpreter(functions, patterns, "InMain", stream, false);

        System.out.print("<article>");

        String tagName = null;
        int valueCount = 0;

        while (interpreter.hasNext() == true)
        {
            JTeroEvent event = interpreter.nextEvent();

            if (event.getCurrentFunctionName().equals("InLaTeXCommandPlanetMathName") == true)
            {
                if (valueCount > 0 &&
                    tagName != null)
                {
                    System.out.print("</" + tagName + ">");
                }

                valueCount = 0;

                tagName = event.getData();
            }
            else if (event.getCurrentFunctionName().equals("InLaTeXCommandValue") == true)
            {
                valueCount += 1;

                if (valueCount == 1)
                {
                    System.out.print("<" + tagName + ">");
                }

                System.out.print("<value-" + valueCount + ">");

                String data = event.getData();
                data = data.replaceAll("&", "&amp;");
                data = data.replaceAll("<", "&lt;");
                data = data.replaceAll(">", "&gt;");

                System.out.print(data);
                System.out.print("</value-" + valueCount + ">");
            }

            if (event.getNextFunctionName().equals("end") == true)
            {
                break;
            }
        }

        if (valueCount > 0 &&
            tagName != null)
        {
            System.out.print("</" + tagName + ">\n");
        }

        System.out.print("</article>");

        return 0;
    }
}
