<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2022 Stephan Kreutzer

This file is part of generator_1 for planetmath.org of
clients for the digital_publishing_workflow_tools.

generator_1 is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License version 3 or any later
version of the license, as published by the Free Software Foundation.

generator_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with generator_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xsl:comment> This file was generated by stylesheet_prepare_1.xsl of generator_1 for planetmath.org of clients for the digital_publishing_workflow_tools, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and https://publishing-systems.org). </xsl:comment><xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="./articles"/>
  </xsl:template>

  <xsl:template match="/articles">
    <articles>
      <xsl:apply-templates select="./article"/>
    </articles>
  </xsl:template>

  <xsl:template match="/articles/article">
    <article>
      <xsl:apply-templates/>
    </article>
  </xsl:template>

  <xsl:template match="/articles/article/canonicalname">
    <canonicalname>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </canonicalname>
  </xsl:template>

  <xsl:template match="/articles/article/created">
    <created>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </created>
  </xsl:template>

  <xsl:template match="/articles/article/modified">
    <modified>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </modified>
  </xsl:template>

  <xsl:template match="/articles/article/owner">
    <owner>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="./value-2/text()"/>
    </owner>
  </xsl:template>

  <xsl:template match="/articles/article/modifier">
    <modifier>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="./value-2/text()"/>
    </modifier>
  </xsl:template>

  <xsl:template match="/articles/article/title">
    <title>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </title>
  </xsl:template>

  <xsl:template match="/articles/article/record">
    <record>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="./value-2/text()"/>
    </record>
  </xsl:template>

  <xsl:template match="/articles/article/privacy">
    <privacy>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </privacy>
  </xsl:template>

  <xsl:template match="/articles/article/author">
    <author>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="./value-2/text()"/>
    </author>
  </xsl:template>

  <xsl:template match="/articles/article/type">
    <type>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </type>
  </xsl:template>

  <xsl:template match="/articles/article/comment">
    <comment>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </comment>
  </xsl:template>

  <xsl:template match="/articles/article/classification">
    <classification>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="./value-2/text()"/>
    </classification>
  </xsl:template>

  <xsl:template match="/articles/article/related">
    <related>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </related>
  </xsl:template>

  <xsl:template match="/articles/article/defines">
    <defines>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </defines>
  </xsl:template>

  <xsl:template match="/articles/article/keywords">
    <keywords>
      <xsl:value-of select="./value-1/text()"/>
      <xsl:if test="./value-2">
        <value-2>
          <xsl:comment> Error: value-2 omitted! </xsl:comment>
        </value-2>
      </xsl:if>
    </keywords>
  </xsl:template>

  <xsl:template match="/articles/article/synonym">
    <synonym>
      <xsl:value-of select="./value-1/text()"/>
      <!-- xsl:value-of select="./value-2/text()"/ -->
      <xsl:if test="./value-2">
        <xsl:comment> Canonical synonym name omitted. </xsl:comment>
      </xsl:if>
    </synonym>
  </xsl:template>

  <xsl:template match="text()|@*|node()"/>

</xsl:stylesheet>
