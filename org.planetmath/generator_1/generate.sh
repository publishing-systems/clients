#!/bin/sh
# Copyright (C) 2022 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > output.xml;
printf "<articles>" >> output.xml;

for filepath in ./11_Number_theory/*.tex
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    java -cp ./jterosta/ JTeroStA $filepath >> output.xml
    #./tero/tero-4/tero ./LaTeXPlanetMath/ InMain $filepath

done

printf "</articles>" >> output.xml;


java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_extract_element_names.xml ./resultinfo_xml_xslt_transformator_1_extract_element_names.xml

sort ./element_names.txt | uniq

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_prepare.xml ./resultinfo_xml_xslt_transformator_1_prepare.xml

java -cp ./digital_publishing_workflow_tools/xml_to_dimgra/xml_to_dimgra_1/ xml_to_dimgra_1 ./jobfile_xml_to_dimgra_1.xml ./resultinfo_xml_to_dimgra_1.xml

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_dimgra_xhtml.xml ./resultinfo_xml_xslt_transformator_1_dimgra_xhtml.xml
