<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2022 Stephan Kreutzer

This file is part of generator_1 for planetmath.org of
clients for the digital_publishing_workflow_tools.

generator_1 is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License version 3 or any later
version of the license, as published by the Free Software Foundation.

generator_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with generator_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

  <xsl:template match="node()">
    <xsl:value-of select="name()"/>
    <xsl:text>
</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
