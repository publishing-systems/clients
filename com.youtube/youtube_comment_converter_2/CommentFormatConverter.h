/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of youtube_comment_converter_2.
 *
 * youtube_comment_converter_2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * youtube_comment_converter_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with youtube_comment_converter_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CommentFormatConverter.h
 * @author Stephan Kreutzer
 * @since 2019-02-23
 */

#ifndef _COMMENTFORMATCONVERTER_H
#define _COMMENTFORMATCONVERTER_H

#include "XMLEventReader.h"
#include "ParsedLink.h"
#include <memory>
#include <istream>
#include <ostream>

typedef std::unique_ptr<cppstax::XMLEventReader> XMLEventReader;



class CommentFormatConverter
{
public:
    CommentFormatConverter(const std::shared_ptr<std::istream>& pInputStream,
                           const std::shared_ptr<std::ostream>& pOutputStream);
    ~CommentFormatConverter();

public:
    int Convert();

protected:
    int HandleCommentHistoryEntry();
    int HandleFormattedString(const bool bPostExpander);
    std::unique_ptr<ParsedLink> HandleLink(const cppstax::StartElement& aStartElement);
    int HandleExpander();
    int HandleCommentText();
    int HandleCommentTextLink(const cppstax::StartElement& aStartElement);

protected:
    int ConsumeToEndElement(const std::string& strEndElementName);
    int OutputXmlEscape(const std::string& strInput, bool bForAttribute);

protected:
    std::shared_ptr<std::istream> m_pInputStream;
    std::shared_ptr<std::ostream> m_pOutputStream;
    XMLEventReader m_pReader;

};

#endif
