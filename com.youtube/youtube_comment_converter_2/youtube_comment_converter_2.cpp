/* Copyright (C) 2017-2019 Stephan Kreutzer
 *
 * This file is part of youtube_comment_converter_2.
 *
 * youtube_comment_converter_2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * youtube_comment_converter_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with youtube_comment_converter_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/youtube_comment_converter_2.cpp
 * @author Stephan Kreutzer
 * @since 2017-08-28
 */

#include "CommentFormatConverter.h"
#include <memory>
#include <iostream>
#include <fstream>



int main(int argc, char* argv[])
{
    std::cout << "youtube_comment_converter_2 Copyright (C) 2017-2019 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/publishing-systems/clients/ and\n"
              << "the project website http://www.publishing-systems.org.\n"
              << std::endl;

    std::shared_ptr<std::ifstream> pInputStream = nullptr;
    std::shared_ptr<std::ofstream> pOutputStream = nullptr;

    try
    {
        if (argc >= 3)
        {
            pInputStream = std::shared_ptr<std::ifstream>(new std::ifstream);
            // "Inner HTML" of <div/> with ID #contents and class .ytd-section-list-renderer
            // copied from F12/Firebug on https://www.youtube.com/feed/history/comment_history
            // after login and loading all entries.
            pInputStream->open(argv[1]);

            if (pInputStream->is_open() != true)
            {
                std::cout << "Couldn't open input file '" << argv[1] << "'.";
                return -1;
            }

            pOutputStream = std::unique_ptr<std::ofstream>(new std::ofstream);
            pOutputStream->open(argv[2]);

            if (pOutputStream->is_open() != true)
            {
                std::cout << "Couldn't open output file '" << argv[2] << "'.";
                pOutputStream->close();
                return -1;
            }

            CommentFormatConverter aConverter(pInputStream, pOutputStream);
            aConverter.Convert();

            pOutputStream->close();
            pInputStream->close();
        }
        else
        {
            std::cout << "Usage:\n\tyoutube_comment_converter_2 <input-xml-file> <output-xml-file>\n" << std::endl;
            return 1;
        }
    }
    catch (std::exception* pException)
    {
        std::cout << "Exception: " << pException->what() << std::endl;

        if (pOutputStream != nullptr)
        {
            if (pOutputStream->is_open() == true)
            {
                pOutputStream->close();
            }
        }

        if (pInputStream != nullptr)
        {
            if (pInputStream->is_open() == true)
            {
                pInputStream->close();
            }
        }

        return -1;
    }

    return 0;
}
