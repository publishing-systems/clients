/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of youtube_comment_converter_2.
 *
 * youtube_comment_converter_2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * youtube_comment_converter_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with youtube_comment_converter_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/ParsedLink.cpp
 * @author Stephan Kreutzer
 * @since 2019-02-22
 */

#include "ParsedLink.h"

ParsedLink::ParsedLink(const std::string& strCaption,
                       const std::string& strReference):
  m_strCaption(strCaption),
  m_strReference(strReference)
{

}

ParsedLink::~ParsedLink()
{

}

const std::string& ParsedLink::GetCaption()
{
    return m_strCaption;
}

const std::string& ParsedLink::GetReference()
{
    return m_strReference;
}
