/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of youtube_video_id_scanner_1.
 *
 * youtube_video_id_scanner_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * youtube_video_id_scanner_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with youtube_video_id_scanner_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/youtube_video_id_scanner_1.java
 * @brief Example program dealing with combinatorics/permutation,
 *     demonstrating a scan for YouTube video IDs.
 * @author Stephan Kreutzer
 * @since 2021-02-18
 */



import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.io.DataInputStream;



public class youtube_video_id_scanner_1
{
    public static void main(String args[])
    {
        System.out.print("youtube_video_id_scanner_1 Copyright (C) 2021 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/clients/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        youtube_video_id_scanner_1 instance = new youtube_video_id_scanner_1();
        instance.run();
    }

    public int run()
    {
        try
        {
            String start = new String("-----------");
            int length = start.length();
            // ASCII 26 + 26 + 10 + 1 + 1 = 64: A-Z a-z 0-9 - _
            BigInteger base = new BigInteger("64");
            BigInteger max = base.pow(length);
            BigInteger zero = new BigInteger("0");
            BigInteger one = new BigInteger("1");

            byte[] characters = start.getBytes("UTF-8");
            String videoId = start;

            int[] mask = new int[length];

            if (mask.length != characters.length)
            {
                throw new RuntimeException("Length of mask (" + mask.length + ") differs from length of characters (" + characters.length + ").");
            }

            for (int i = 0; i < length; i++)
            {
                if (start.charAt(i) == 45)
                {
                    mask[i] = 0;
                }
                else if (start.charAt(i) >= 48 && start.charAt(i) <= 57)
                {
                    mask[i] = 1 + (start.charAt(i) - 48);
                }
                else if (start.charAt(i) >= 65 && start.charAt(i) <= 90)
                {
                    mask[i] = 1 + 10 + (start.charAt(i) - 65);
                }
                else if (start.charAt(i) == 95)
                {
                    mask[i] = 1 + 10 + 26 + 0;
                }
                else if (start.charAt(i) >= 97 && start.charAt(i) <= 122)
                {
                    mask[i] = 1 + 10 + 26 + 1 + (start.charAt(i) - 97);
                }
                else
                {
                    char character = start.charAt(i);

                    throw new RuntimeException("Invalid character '" + character + "' (" + (int)character + ", " + String.format("0x%X", (int)character) + ") in start video ID \"" + start + "\".");
                }
            }


            for (BigInteger i = zero; i.compareTo(max) < 0; i = i.add(one))
            {
                this.request(videoId);


                /*
                if (i.mod(base).compareTo(zero) == 0)
                {

                }
                */


                int pos = 0;

                for (pos = 0; pos < length; pos++)
                {
                    if (mask[pos] == 63)
                    {
                        mask[pos] = 0;
                        continue;
                    }
                    else
                    {
                        mask[pos] += 1;
                        break;
                    }
                }


                /*
                for (int j = 0; j < length; j++)
                {
                    System.out.print(mask[j] + ", ");
                }

                System.out.print("= " + pos + "\n");
                */


                for (int j = 0, target = pos + 1; j < target; j++)
                {
                    if (j >= characters.length)
                    {
                        // Overflow!
                        break;
                    }

                    if (characters[j] == 45)
                    {
                        characters[j] = 48;
                    }
                    else if (characters[j] >= 48 && characters[j] <= 57)
                    {
                        if (characters[j] >= 57)
                        {
                            characters[j] = 65;
                        }
                        else
                        {
                            characters[j] += 1;
                        }
                    }
                    else if (characters[j] >= 65 && characters[j] <= 90)
                    {
                        if (characters[j] >= 90)
                        {
                            characters[j] = 95;
                        }
                        else
                        {
                            characters[j] += 1;
                        }
                    }
                    else if (characters[j] == 95)
                    {
                        characters[j] = 97;
                    }
                    else if (characters[j] >= 97 && characters[j] <= 122)
                    {
                        if (characters[j] >= 122)
                        {
                            characters[j] = 45;
                        }
                        else
                        {
                            characters[j] += 1;
                        }
                    }
                }


                videoId = new String(characters, "UTF-8");

                if (videoId.equals(start) == true)
                {
                    break;
                }

                Thread.sleep(5000);
            }
        }
        catch (InterruptedException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }

        return 0;
    }

    public int request(String videoId) throws
      UnsupportedEncodingException,
      IOException
    {
        System.out.print(videoId);


        String requestURL = "https://www.youtube.com/watch?v=" + videoId;
        String requestMethod = "GET";
        Map<String, String> requestHeaderFields = null;


        HttpsURLConnection connection = null;

        try
        {
            URL url = new URL(requestURL);

            connection = (HttpsURLConnection)url.openConnection();

            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(requestMethod);
            connection.setUseCaches(false);

            if (requestHeaderFields != null)
            {
                if (requestHeaderFields.isEmpty() != true)
                {
                    for (Map.Entry<String, String> field : requestHeaderFields.entrySet())
                    {
                        if (field.getKey().equals("User-Agent") == true)
                        {
                            continue;
                        }

                        connection.setRequestProperty(field.getKey(), field.getValue());
                    }
                }
            }

            connection.setRequestProperty("User-Agent", "RNSLla4Ixc0Rz8zfqot36YDfSqC8Xygi");

            connection.connect();
        }
        catch (MalformedURLException ex)
        {
            throw ex;
        }
        catch (IOException ex)
        {
            throw ex;
        }

        if (connection != null)
        {
            int responseCode = -1;

            try
            {
                responseCode = connection.getResponseCode();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            System.out.print("," + responseCode);

            if (responseCode != 200)
            {
                throw new RuntimeException("HTTP response code was not 200, but " + responseCode + " instead.");
            }

            byte[] data = new byte[1024];

            DataInputStream reader = null;

            boolean error = false;
            boolean exception = false;

            try
            {
                reader = new DataInputStream(connection.getInputStream());
            }
            catch (IOException ex)
            {
                error = true;
            }

            if (error == true)
            {
                if (connection.getErrorStream() != null)
                {
                    reader = new DataInputStream(connection.getErrorStream());
                    error = false;
                }
            }

            if (error == false)
            {
                String stringUnlisted = new String("<meta itemprop=\"unlisted\" content=\"True\">");
                int posStringUnlisted = 0;
                String stringListed = new String("<meta itemprop=\"unlisted\" content=\"False\">");
                int posStringListed = 0;
                boolean found = false;

                try
                {
                    int bytesRead = reader.read(data, 0, data.length);

                    while (bytesRead > 0)
                    {
                        for (int i = 0; i < bytesRead; i++)
                        {
                            if ((char)data[i] == stringUnlisted.charAt(posStringUnlisted))
                            {
                                posStringUnlisted += 1;

                                if (posStringUnlisted >= stringUnlisted.length())
                                {
                                    System.out.print(",unlisted");
                                    found = true;
                                    break;
                                }
                            }
                            else
                            {
                                posStringUnlisted = 0;
                            }

                            if ((char)data[i] == stringListed.charAt(posStringListed))
                            {
                                posStringListed += 1;

                                if (posStringListed >= stringListed.length())
                                {
                                    System.out.print(",listed");
                                    found = true;
                                    break;
                                }
                            }
                            else
                            {
                                posStringListed = 0;
                            }
                        }

                        if (found == true)
                        {
                            break;
                        }

                        bytesRead = reader.read(data, 0, data.length);
                    }
                }
                catch (IOException ex)
                {
                    error = true;
                    exception = true;
                    throw ex;
                }
                finally
                {
                    if (reader != null)
                    {
                        try
                        {
                            reader.close();
                            reader = null;
                        }
                        catch (IOException ex)
                        {
                            if (exception == false)
                            {
                                exception = true;
                                throw ex;
                            }
                        }
                        finally
                        {
                            reader = null;
                        }
                    }
                }

                if (found == false)
                {
                    System.out.print(",not-found");
                }
            }
        }

        System.out.print("\r\n");

        return 0;
    }
}
