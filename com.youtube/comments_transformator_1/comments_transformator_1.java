/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of comments_transformator_1.
 *
 * comments_transformator_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * comments_transformator_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with comments_transformator_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/comments_transformator_1.java
 * @author Stephan Kreutzer
 * @since 2019-04-09
 */



import java.io.File;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import java.io.FileNotFoundException;
import javax.xml.stream.XMLStreamException;
import java.util.Map;
import java.util.HashMap;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Scanner;



public class comments_transformator_1
{
    public static void main(String args[])
    {
        System.out.print("comments_transformator_1\n" +
                         "Copyright (C) 2019 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/clients/ and the project website\n" +
                         "http://www.publishing-systems.org.\n\n");

        if (args.length < 2)
        {
            System.out.println("Usage:\n\tcomments_transformator_1 input-file output-directory\n");
            System.exit(1);
        }


        String programPath = comments_transformator_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        File tempDirectory = new File(programPath + "temp");

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    System.out.println("comment_transformator_1: Temporary directory '" + tempDirectory.getAbsolutePath() + "' isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("comment_transformator_1: Temporary path '" + tempDirectory.getAbsolutePath() + "' isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }
        }

        File inputFile = new File(args[0]);

        if (inputFile.exists() != true)
        {
            System.out.println("comments_transformator_1: Input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.out.println("comments_transformator_1: Input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.out.println("comments_transformator_1: Input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        File outputDirectory = new File(args[1]);

        if (outputDirectory.exists() == true)
        {
            if (outputDirectory.isDirectory() != true)
            {
                System.out.println("comments_transformator_1: Output path \"" + outputDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }

            if (outputDirectory.canWrite() != true)
            {
                System.out.println("comments_transformator_1: Output directory \"" + outputDirectory.getAbsolutePath() + "\" isn't writable.");
                System.exit(1);
            }
        }
        else
        {
            outputDirectory.mkdirs();
        }

        System.out.println("comments_transformator_1: Call with input file \"" + inputFile.getAbsolutePath() + "\" and output directory \"" + outputDirectory.getAbsolutePath() + "\".");


        Map<String, Integer> outputFileMapping = new HashMap<String, Integer>();
        File outputIndexFile = new File(outputDirectory.getAbsolutePath() + File.separator + "index.xml");

        try
        {
            BufferedWriter writerOutputIndex = new BufferedWriter(
                                               new OutputStreamWriter(
                                               new FileOutputStream(outputIndexFile),
                                               "UTF-8"));

            writerOutputIndex.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writerOutputIndex.write("<!-- This file was generated by youtube_comments_transformator_1 of clients for the automated_digital_publishing and digital_publishing_workflow_tools packages, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/tree/master/com.youtube/comments_transformator_1/ and http://www.publishing-systems.org). -->\n");
            writerOutputIndex.write("<comment-history-index xmlns=\"htx-scheme-id://org.publishing-systems.20140527T120137Z/digital_publishing_workflow_tools/clients/comments_index.20190411T000000Z\">\n");

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(inputFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in, "UTF8");

                XMLEvent event = null;
                Integer commentCount = 1;

                while (eventReader.hasNext() == true)
                {
                    event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        QName elementName = event.asStartElement().getName();
                        String elementNameString = elementName.getLocalPart();

                        if (elementNameString.equalsIgnoreCase("comment-history-entry") == true)
                        {
                            CommentHistoryEntryParser commentEntry = new CommentHistoryEntryParser(eventReader);

                            if (commentEntry.ParseEntry() != 0)
                            {
                                System.exit(1);
                            }

                            int targetNumber = 0;
                            boolean firstEncounter = false;

                            if (outputFileMapping.containsKey(commentEntry.GetSourceReference()) != true)
                            {
                                outputFileMapping.put(commentEntry.GetSourceReference(), commentCount);
                                targetNumber = commentCount;
                                firstEncounter = true;
                                ++commentCount;
                            }
                            else
                            {
                                targetNumber = outputFileMapping.get(commentEntry.GetSourceReference());
                            }

                            try
                            {
                                File outputXmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + targetNumber + ".xml");

                                if (firstEncounter == true &&
                                    outputXmlFile.exists() == true)
                                {
                                    // This is dangerous because the writer would append to
                                    // it, and the file might already contain entirely different
                                    // data or entries from a previous call of this tool.
                                    System.out.println("comments_transformator_1: Output file \"" + outputXmlFile.getAbsolutePath() + "\" does already exist.");
                                    System.exit(1);
                                }

                                BufferedWriter writerOutputXml = new BufferedWriter(
                                                                 new OutputStreamWriter(
                                                                 new FileOutputStream(outputXmlFile, true),
                                                                 "UTF-8"));

                                if (firstEncounter == true)
                                {
                                    writerOutputXml.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                                    writerOutputXml.write("<comment-history xmlns=\"htx-scheme-id://org.publishing-systems.20140527T120137Z/digital_publishing_workflow_tools/clients/comments_per_id_format.20190410T000000Z\">\n");
                                    writerOutputXml.write("<!-- This file was generated by youtube_comments_transformator_1 of clients for the automated_digital_publishing and digital_publishing_workflow_tools packages, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/tree/master/com.youtube/comments_transformator_1/ and http://www.publishing-systems.org). -->\n");

                                    /**
                                     * @todo This isn't checking if <source-reference/> or <source-name/> are
                                     *     differing between individual comment entries!
                                     */

                                    if (commentEntry.GetSourceReference() != null)
                                    {
                                        writerOutputXml.write("<source-reference>");

                                        String escapedString = commentEntry.GetSourceReference();

                                        // Ampersand needs to be the first, otherwise it would double-encode
                                        // other entities.
                                        escapedString = escapedString.replaceAll("&", "&amp;");
                                        escapedString = escapedString.replaceAll("<", "&lt;");
                                        escapedString = escapedString.replaceAll(">", "&gt;");

                                        writerOutputXml.write(escapedString);
                                        writerOutputXml.write("</source-reference>");
                                    }
                                    else
                                    {
                                        System.out.println("comments_transformator_1: No source reference.");
                                        System.exit(1);
                                    }

                                    if (commentEntry.GetSourceName() != null)
                                    {
                                        writerOutputXml.write("<source-name>");

                                        String escapedString = commentEntry.GetSourceName();

                                        // Ampersand needs to be the first, otherwise it would double-encode
                                        // other entities.
                                        escapedString = escapedString.replaceAll("&", "&amp;");
                                        escapedString = escapedString.replaceAll("<", "&lt;");
                                        escapedString = escapedString.replaceAll(">", "&gt;");

                                        writerOutputXml.write(escapedString);
                                        writerOutputXml.write("</source-name>");
                                    }
                                    else
                                    {
                                        System.out.println("comments_transformator_1: No source name.");
                                        System.exit(1);
                                    }
                                }

                                writerOutputXml.write("<comment-history-entry>");

                                if (commentEntry.GetCommentReference() != null)
                                {
                                    writerOutputXml.write("<comment-reference>");

                                    String escapedString = commentEntry.GetCommentReference();

                                    // Ampersand needs to be the first, otherwise it would double-encode
                                    // other entities.
                                    escapedString = escapedString.replaceAll("&", "&amp;");
                                    escapedString = escapedString.replaceAll("<", "&lt;");
                                    escapedString = escapedString.replaceAll(">", "&gt;");

                                    writerOutputXml.write(escapedString);
                                    writerOutputXml.write("</comment-reference>");
                                }
                                else if (commentEntry.GetCommentReferenceReply() != null &&
                                         commentEntry.GetCommentReferenceOrigin() != null)
                                {
                                    writerOutputXml.write("<comment-reference-reply>");

                                    String escapedString = commentEntry.GetCommentReferenceReply();

                                    // Ampersand needs to be the first, otherwise it would double-encode
                                    // other entities.
                                    escapedString = escapedString.replaceAll("&", "&amp;");
                                    escapedString = escapedString.replaceAll("<", "&lt;");
                                    escapedString = escapedString.replaceAll(">", "&gt;");

                                    writerOutputXml.write(escapedString);
                                    writerOutputXml.write("</comment-reference-reply>");

                                    writerOutputXml.write("<comment-reference-origin>");

                                    escapedString = commentEntry.GetCommentReferenceOrigin();

                                    // Ampersand needs to be the first, otherwise it would double-encode
                                    // other entities.
                                    escapedString = escapedString.replaceAll("&", "&amp;");
                                    escapedString = escapedString.replaceAll("<", "&lt;");
                                    escapedString = escapedString.replaceAll(">", "&gt;");

                                    writerOutputXml.write(escapedString);
                                    writerOutputXml.write("</comment-reference-origin>");
                                }
                                else
                                {
                                    System.out.println("comments_transformator_1: Comment reference incomplete.");
                                    System.exit(1);
                                }

                                if (commentEntry.GetCommentText() != null)
                                {
                                    writerOutputXml.write("<comment-text>");

                                    // Special XML characters already escaped!
                                    writerOutputXml.write(commentEntry.GetCommentText());

                                    writerOutputXml.write("</comment-text>");
                                }
                                else
                                {
                                    System.out.println("comments_transformator_1: No comment text.");
                                    System.exit(1);
                                }

                                writerOutputXml.write("</comment-history-entry>\n");
                                writerOutputXml.flush();
                                writerOutputXml.close();
                            }
                            catch (FileNotFoundException ex)
                            {
                                ex.printStackTrace();
                                System.exit(1);
                            }
                            catch (UnsupportedEncodingException ex)
                            {
                                ex.printStackTrace();
                                System.exit(1);
                            }
                            catch (IOException ex)
                            {
                                ex.printStackTrace();
                                System.exit(1);
                            }

                            if (firstEncounter == true)
                            {
                                writerOutputIndex.write("  <comment-file source-path=\"" + targetNumber + ".xml\" xhtml-path=\"" + targetNumber + ".xhtml\" source-name=\"");

                                String escapedString = commentEntry.GetSourceName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                escapedString = escapedString.replaceAll("&", "&amp;");
                                escapedString = escapedString.replaceAll("<", "&lt;");
                                escapedString = escapedString.replaceAll(">", "&gt;");
                                escapedString = escapedString.replaceAll("\"", "&quot;");
                                escapedString = escapedString.replaceAll("'", "&apos;");

                                writerOutputIndex.write(escapedString);

                                writerOutputIndex.write("\"/>\n");
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }
            catch (XMLStreamException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            writerOutputIndex.write("</comment-history-index>\n");
            writerOutputIndex.flush();
            writerOutputIndex.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }


        File xsltTransformatorDirectory = new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "digital_publishing_workflow_tools" + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1");
        File xsltTransformatorJobfile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1.xml");
        File xsltTransformatorResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1.xml");

        try
        {
            BufferedWriter writerTransformatorJobfile = new BufferedWriter(
                                                        new OutputStreamWriter(
                                                        new FileOutputStream(xsltTransformatorJobfile),
                                                        "UTF-8"));

            writerTransformatorJobfile.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writerTransformatorJobfile.write("<!-- This file was generated by youtube_comments_transformator_1 of clients for the automated_digital_publishing and digital_publishing_workflow_tools packages, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/tree/master/com.youtube/comments_transformator_1/ and http://www.publishing-systems.org). -->\n");
            writerTransformatorJobfile.write("<xml-xslt-transformator-1-jobfile>\n");

            for (Map.Entry<String, Integer> entry : outputFileMapping.entrySet())
            {
                File outputXmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + entry.getValue() + ".xml");

                try
                {
                    BufferedWriter writerOutputXml = new BufferedWriter(
                                                     new OutputStreamWriter(
                                                     new FileOutputStream(outputXmlFile, true),
                                                     "UTF-8"));

                    writerOutputXml.write("</comment-history>");

                    writerOutputXml.flush();
                    writerOutputXml.close();
                }
                catch (FileNotFoundException ex)
                {
                    ex.printStackTrace();
                    System.exit(1);
                }
                catch (UnsupportedEncodingException ex)
                {
                    ex.printStackTrace();
                    System.exit(1);
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                    System.exit(1);
                }

                writerTransformatorJobfile.write("  <job input-file=\"" + outputXmlFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + xsltTransformatorDirectory + File.separator + "entities" + File.separator + "config_empty.xml"+ "\" stylesheet-file=\"" + programPath + File.separator + "stylesheet-comment-file-xhtml.xsl\" output-file=\"" + outputDirectory.getAbsolutePath() + File.separator + entry.getValue() + ".xhtml\"/>");
            }

            writerTransformatorJobfile.write("  <job input-file=\"" + outputDirectory.getAbsolutePath() + File.separator + "index.xml\" entities-resolver-config-file=\"" + xsltTransformatorDirectory + File.separator + "entities" + File.separator + "config_empty.xml"+ "\" stylesheet-file=\"" + programPath + File.separator + "stylesheet-index-file-xhtml.xsl\" output-file=\"" + outputDirectory.getAbsolutePath() + File.separator + "index.xhtml\"/>");
            writerTransformatorJobfile.write("</xml-xslt-transformator-1-jobfile>\n");
            writerTransformatorJobfile.flush();
            writerTransformatorJobfile.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xsltTransformatorJobfile.getAbsolutePath(), xsltTransformatorResultInfoFile.getAbsolutePath());
        builder.directory(xsltTransformatorDirectory);
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}

