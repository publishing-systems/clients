/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of comments_transformator_1
 *
 * comments_transformator_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * comments_transformator_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with comments_transformator_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/CommentHistoryEntryParser.java
 * @author Stephan Kreutzer
 * @since 2019-04-09
 */



import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLStreamException;
import java.util.Iterator;
import javax.xml.stream.events.Attribute;



public class CommentHistoryEntryParser
{
    public CommentHistoryEntryParser(XMLEventReader reader)
    {
        this.eventReader = reader;
    }

    public int ParseEntry() throws XMLStreamException
    {
        XMLEvent event = null;

        while (this.eventReader.hasNext() == true)
        {
            event = this.eventReader.nextEvent();

            if (event.isStartElement() == true)
            {
                QName elementName = event.asStartElement().getName();
                String elementNameString = elementName.getLocalPart();

                if (elementNameString.equals("comment-reference") == true)
                {
                    this.commentReference = GetTextNodes("comment-reference");
                }
                else if (elementNameString.equals("comment-reference-reply") == true)
                {
                    this.commentReferenceReply = GetTextNodes("comment-reference-reply");
                }
                else if (elementNameString.equals("comment-reference-origin") == true)
                {
                    this.commentReferenceOrigin = GetTextNodes("comment-reference-origin");
                }
                else if (elementNameString.equals("source-reference") == true)
                {
                    this.sourceReference = GetTextNodes("source-reference");
                }
                else if (elementNameString.equals("source-name") == true)
                {
                    this.sourceName = GetTextNodes("source-name");
                }
                else if (elementNameString.equals("comment-text") == true)
                {
                    this.commentText = ParseCommentText();
                }
            }
            else if (event.isEndElement() == true)
            {
                if (event.asEndElement().getName().getLocalPart().equals("comment-history-entry") == true)
                {
                    break;
                }
            }
        }

        if (this.commentReference == null &&
            (this.commentReferenceReply == null ||
             this.commentReferenceOrigin == null))
        {
            System.out.println("comments_transformator_1: Comment reference incomplete.");
            return -1;
        }

        if (this.sourceReference == null)
        {
            System.out.println("comments_transformator_1: 'source-reference' is missing in 'comment-history-entry'.");
            return -1;
        }

        if (this.sourceName == null)
        {
            System.out.println("comments_transformator_1: 'source-name' is missing in 'comment-history-entry'.");
            return -1;
        }

        if (this.commentText == null)
        {
            System.out.println("comments_transformator_1: 'comment-text' is missing in 'comment-history-entry'.");
            return -1;
        }

        return 0;
    }

    protected String ParseCommentText() throws XMLStreamException
    {
        StringBuilder sbText = new StringBuilder();

        while (this.eventReader.hasNext() == true)
        {
            XMLEvent event = this.eventReader.nextEvent();

            if (event.isStartElement() == true)
            {
                QName elementName = event.asStartElement().getName();

                sbText.append("<");
                sbText.append(elementName.getLocalPart());

                // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                @SuppressWarnings("unchecked")
                Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                while (attributes.hasNext() == true)
                {
                    Attribute attribute = attributes.next();
                    QName attributeName = attribute.getName();
                    String fullAttributeName = attributeName.getLocalPart();

                    /**
                     * @todo Make tool XML namespace aware.
                     */
                    /*
                    if (attributeName.getPrefix().length() > 0)
                    {
                        fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                    }
                    */

                    String attributeValue = attribute.getValue();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    attributeValue = attributeValue.replaceAll("&", "&amp;");
                    attributeValue = attributeValue.replaceAll("\"", "&quot;");
                    attributeValue = attributeValue.replaceAll("'", "&apos;");
                    attributeValue = attributeValue.replaceAll("<", "&lt;");
                    attributeValue = attributeValue.replaceAll(">", "&gt;");

                    sbText.append(" ");
                    sbText.append(fullAttributeName);
                    sbText.append("=\"");
                    sbText.append(attributeValue);
                    sbText.append("\"");
                }

                sbText.append(">");
            }
            else if (event.isCharacters() == true)
            {
                String characters = event.asCharacters().getData();

                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                characters = characters.replaceAll("&", "&amp;");
                characters = characters.replaceAll("<", "&lt;");
                characters = characters.replaceAll(">", "&gt;");

                sbText.append(characters);
            }
            else if (event.isEndElement() == true)
            {
                QName elementName = event.asEndElement().getName();
                String elementNameText = elementName.getLocalPart();

                if (event.asEndElement().getName().getLocalPart().equals("comment-text") == true)
                {
                    break;
                }
                else
                {
                    sbText.append("</");
                    sbText.append(elementNameText);
                    sbText.append(">");
                }
            }
        }

        return sbText.toString();
    }

    protected String GetTextNodes(String endElementName) throws XMLStreamException
    {
        StringBuilder sbText = new StringBuilder();

        while (this.eventReader.hasNext() == true)
        {
            XMLEvent event = this.eventReader.nextEvent();

            if (event.isCharacters() == true)
            {
                sbText.append(event.asCharacters().getData());
            }
            else if (event.isEndElement() == true)
            {
                if (event.asEndElement().getName().getLocalPart().equals(endElementName) == true)
                {
                    break;
                }
            }
        }

        return sbText.toString();
    }

    public String GetCommentReference()
    {
        return this.commentReference;
    }

    public String GetCommentReferenceReply()
    {
        return this.commentReferenceReply;
    }

    public String GetCommentReferenceOrigin()
    {
        return this.commentReferenceOrigin;
    }

    public String GetSourceReference()
    {
        return this.sourceReference;
    }

    public String GetSourceName()
    {
        return this.sourceName;
    }

    /**
     * @details Special XML characters already escaped!
     */
    public String GetCommentText()
    {
        return this.commentText;
    }


    protected XMLEventReader eventReader;

    protected String commentReference = null;
    protected String commentReferenceReply = null;
    protected String commentReferenceOrigin = null;

    protected String sourceReference = null;
    protected String sourceName = null;
    protected String commentText = null;
}
