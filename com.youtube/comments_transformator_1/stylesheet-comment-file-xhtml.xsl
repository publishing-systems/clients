<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2019 Stephan Kreutzer

This file is part of comments_transformator_1.

comments_transformator_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

comments_transformator_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with comments_transformator_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:in="htx-scheme-id://org.publishing-systems.20140527T120137Z/digital_publishing_workflow_tools/clients/comments_per_id_format.20190410T000000Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/in:comment-history">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
        <xsl:comment> This file was generated by stylesheet-comment-file-xhtml.xsl of youtube_comments_transformator_1 of clients for the automated_digital_publishing and digital_publishing_workflow_tools packages, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/tree/master/com.youtube/comments_transformator_1/ and http://www.publishing-systems.org). </xsl:comment>
        <title><xsl:value-of select="./in:source-name//text()"/></title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      </head>
      <body>
        <div>
          <h1><xsl:value-of select="./in:source-name//text()"/></h1>
          <p>
            Source Reference: <span class="source-reference"><xsl:value-of select="./in:source-reference//text()"/></span>
          </p>
          <div class="index-link-top">
            <a href="index.xhtml">Index</a>
          </div>
          <xsl:apply-templates/>
          <div class="index-link-bottom">
            <a href="index.xhtml">Index</a>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry">
    <div class="comment-history-entry">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry/in:comment-reference |
                       /in:comment-history/in:comment-history-entry/in:comment-reference-reply">
    <p>
      Comment Reference: <span class="comment-reference"><xsl:value-of select=".//text()"/></span>
    </p>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry/in:comment-reference-origin">
    <p>
      In Reply To: <span class="comment-reference-replying-to"><xsl:value-of select=".//text()"/></span>
    </p>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry/in:comment-text">
    <p class="comment-text">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry/in:comment-text//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/in:comment-history/in:comment-history-entry/in:comment-text//in:a[@class='timecode-link']">
    <a href="{@href}" class="timecode-link"><xsl:value-of select=".//text()"/></a>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
