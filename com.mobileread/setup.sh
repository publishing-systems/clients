#!/bin/sh
make
wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip
unzip automated_digital_publishing-master.zip
rm automated_digital_publishing-master.zip
mv automated_digital_publishing-master automated_digital_publishing
cd automated_digital_publishing
make
cd workflows
java setup1
cd ..
cd ..
mv automated_digital_publishing ./../../
# This may face a timeout because of https://www.w3.org/blog/systeam/2008/02/08/w3c_s_excessive_dtd_traffic/ as the unique namespace identifier isn't supposed to be the retrievable ID of a remote resource, but I'm not going to do any special packaging for Transitional DTDs, so this is a quick, lazy hack.
wget http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd
mv xhtml1-transitional.dtd ./../../automated_digital_publishing/xsltransformator/xsltransformator1/entities/
wget https://github.com/w3c/epubcheck/releases/download/v4.2.2/epubcheck-4.2.2.zip
unzip epubcheck-4.2.2.zip
mv ./epubcheck-4.2.2/* ./../../automated_digital_publishing/epubcheck/epubcheck1/
rm -r ./epubcheck-4.2.2/
rm epubcheck-4.2.2.zip
