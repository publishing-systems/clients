<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2021 Stephan Kreutzer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:graphml="http://graphml.graphdrawing.org/xmlns">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by graphml_to_graph_1.xsl, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://publishing-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2021 Stephan Kreutzer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="graphml-input"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>Graph</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <style type="text/css">
<xsl:text>
body
{
    font-family: sans-serif;
}

#node
{
    width: 62%;
    position: absolute;
    left: 2%;
    text-align: left;
}

#node-heading
{
    text-align: right;
    padding-right: 10%;
}

#edges
{
    width: 32%;
    position: absolute;
    left: 66%;
    text-align: left;
}
</xsl:text>
        </style>
        <script type="text/javascript">
<!-- currentNode ID could be generated into the JavaScript by this XSLT. -->
<xsl:text>
"use strict";

// As browsers/W3C/WHATWG are incredibly evil, they might ignore the self-declarative
// XML namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

// -- Loader --

function GraphLoader()
{
    let that = this;

    that.getCurrentNode = function()
    {
        if (_loaded == false)
        {
            _load();
        }

        if (_nodes.size &gt; 0)
        {
            // JavaScript is insane!!!
            const iter = _nodes[Symbol.iterator]()
            // [0] is probably key, [1] probably value.
            return iter.next().value[1];
        }
        else
        {
            return null;
        }
    }

    that.getNodeById = function(id)
    {
        if (_loaded == false)
        {
            _load();
        }

        let result = _nodes.get(id);

        if (result !== undefined)
        {
            return result;
        }
        else
        {
            return null;
        }
    }

    that.getNodes = function()
    {
        if (_loaded == false)
        {
            _load();
        }

        if (_nodes.size &gt; 0)
        {
            /*
            // Cache this?

            let nodes = new Array();

            for (let iter of _nodes)
            {
                // [0] is probably key, [1] probably value.
                nodes.push(iter[1]);
            }

            return nodes;
            */

            return _nodes;
        }
        else
        {
            return null;
        }
    }

    that.getEdges = function()
    {
        if (_loaded == false)
        {
            _load();
        }

        if (_edges.length &gt; 0)
        {
            return _edges;
        }
        else
        {
            return null;
        }
    }

    function _load()
    {
        if (_loaded != false)
        {
            return 0;
        }

        if ("querySelectorAll" in document)
        {
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            let nodes = document.querySelectorAll("node");

            for (let i = 0, max = nodes.length; i &lt; max; i++)
            {
                if (nodes[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (nodes[i].hasAttribute("id") != true)
                {
                    throw "Embedded GraphML data: Element '" + nodes[i].tagName + "' is missing its 'id' attribute.";
                }

                let id = nodes[i].getAttribute("id");
                let title = null;
                let text = new Array();

                if (id.length &lt;= 0)
                {
                    throw "Embedded GraphML data: Empty 'id' attribute of '" + nodes[i].tagName + "'.";
                }

                let inputNode = nodes[i].children;

                if (inputNode.length &gt; 0)
                {
                    for (let iNode = 0, maxNode = inputNode.length; iNode &lt; maxNode; iNode++)
                    {
                        // TODO: Hard-coded here, replace.
                        if (inputNode[iNode].tagName != "graphml:data")
                        {
                            continue;
                        }

                        if (inputNode[iNode].hasAttribute("key") != true)
                        {
                            throw "Embedded GraphML data: Element '" + inputNode[iNode].tagName + "' is missing its 'key' attribute.";
                        }

                        // TODO: Hard-coded here, replace.
                        if (inputNode[iNode].getAttribute("key") == "title")
                        {
                            title = inputNode[iNode].textContent;
                        }
                        else if (inputNode[iNode].getAttribute("key") == "text")
                        {
                            text.push(inputNode[iNode].textContent);
                        }
                    }
                }

                if (title == null)
                {
                    throw "Embedded GraphML data: Node without title data.";
                }

                _nodes.set(id, new Node(id, title, text));
            }

            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            let edges = document.querySelectorAll("edge");

            for (let i = 0, max = edges.length; i &lt; max; i++)
            {
                if (edges[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (edges[i].hasAttribute("source") != true)
                {
                    throw "Embedded GraphML data: Element '" + edges[i].tagName + "' is missing its 'source' attribute.";
                }

                if (edges[i].hasAttribute("target") != true)
                {
                    throw "Embedded GraphML data: Element '" + edges[i].tagName + "' is missing its 'target' attribute.";
                }

                _edges.push(new Edge(edges[i].getAttribute("source"), edges[i].getAttribute("target")));
            }

            _loaded = true;
            return 0;
        }


        /*
        // Stupid JavaScript/web/browsers: document.getElementsByTagNameNS()
        // seems to not work with XML namespace prefixes.
        let inputXml = document.getElementsByTagName("graphml:graphml");

        if (inputXml !== null)
        {
            if (inputXml.length &gt; 0)
            {
                inputXml = inputXml[0].children;

                if (inputXml.length &gt; 0)
                {
                    for (let iGraphml = 0, maxGraphml = inputXml.length; iGraphml &lt; maxGraphml; iGraphml++)
                    {
                        if (inputXml[iGraphml].tagName != "graphml:graph")
                        {
                            console.log("Embedded GraphML data: Element '" + inputXml[iGraphml].tagName + "' ignored.");
                            continue;
                        }

                        let inputGraph = inputXml[iGraphml].children;

                        if (inputGraph.length &gt; 0)
                        {
                            for (let iGraph = 0, maxGraph = inputGraph.length; iGraph &lt; maxGraph; iGraph++)
                            {
                                if (inputGraph[iGraph].tagName != "graphml:node" &amp;&amp;
                                    inputGraph[iGraph].tagName != "graphml:edge")
                                {
                                    console.log("Embedded GraphML data: Element '" + inputGraph[iGraph].tagName + "' ignored.");
                                    continue;
                                }

                                if (inputGraph[iGraph].tagName == "graphml:node")
                                {
                                    if (inputGraph[iGraph].hasAttribute("id") != true)
                                    {
                                        throw "Embedded GraphML data: Element '" + inputGraph[iGraph].tagName + "' is missing its 'id' attribute.";
                                    }

                                    let id = inputGraph[iGraph].getAttribute("id");
                                    let title = null;
                                    let text = new Array();

                                    if (id.length &lt;= 0)
                                    {
                                        throw "Embedded GraphML data: Empty 'id' attribute of '" + inputGraph[iGraph].tagName + "'.";
                                    }

                                    let inputNode = inputGraph[iGraph].children;

                                    if (inputNode.length &gt; 0)
                                    {
                                        for (let iNode = 0, maxNode = inputNode.length; iNode &lt; maxNode; iNode++)
                                        {
                                            // TODO: Hard-coded here, replace.
                                            if (inputNode[iNode].tagName != "graphml:data")
                                            {
                                                continue;
                                            }

                                            if (inputNode[iNode].hasAttribute("key") != true)
                                            {
                                                throw "Embedded GraphML data: Element '" + inputNode[iNode].tagName + "' is missing its 'key' attribute.";
                                            }

                                            // TODO: Hard-coded here, replace.
                                            if (inputNode[iNode].getAttribute("key") == "title")
                                            {
                                                title = inputNode[iNode].textContent;
                                            }
                                            else if (inputNode[iNode].getAttribute("key") == "text")
                                            {
                                                text.push(inputNode[iNode].textContent);
                                            }
                                        }
                                    }

                                    if (title == null)
                                    {
                                        throw "Embedded GraphML data: Node without title data.";
                                    }

                                    _nodes.set(id, new Node(id, title, text));
                                }
                                else if (inputGraph[iGraph].tagName == "graphml:edge")
                                {
                                    if (inputGraph[iGraph].hasAttribute("source") != true)
                                    {
                                        throw "Embedded GraphML data: Element '" + inputGraph[iGraph].tagName + "' is missing its 'source' attribute.";
                                    }

                                    if (inputGraph[iGraph].hasAttribute("target") != true)
                                    {
                                        throw "Embedded GraphML data: Element '" + inputGraph[iGraph].tagName + "' is missing its 'target' attribute.";
                                    }

                                    _edges.push(new Edge(inputGraph[iGraph].getAttribute("source"), inputGraph[iGraph].getAttribute("target")));
                                }
                            }
                        }

                        break;
                    }

                    _loaded = true;
                }
                else
                {
                    console.log("Embedded GraphML data: Is empty.");
                }
            }
        }
        */

        return 0;
    }

    let _loaded = false;

    let _nodes = new Map();
    let _edges = new Array();
}

function Node(id, title, text)
{
    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getTitle = function()
    {
        return _title;
    }

    that.getText = function()
    {
        return _text;
    }

    let _id = id;
    let _title = title;
    let _text = text;
}

function Edge(from, to)
{
    let that = this;

    that.getFrom = function()
    {
        return _from;
    }

    that.getTo = function()
    {
        return _to;
    }

    let _from = from;
    let _to = to;
}


// -- Renderer --

function RenderNode(currentNode, nodes, edges)
{
    // TODO: Properly check if nodes is a Map and edges an Array.

    if (currentNode == null)
    {
        return -1;
    }

    let target = document.getElementById("node");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'node'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode(currentNode.getTitle()));
    header.setAttribute("id", "node-heading");
    container.appendChild(header);


    for (let i = 0, max = currentNode.getText().length; i &lt; max; i++)
    {
        let div = document.createElement("div");
        div.appendChild(document.createTextNode(currentNode.getText()[i]));
        container.appendChild(div);
    }

    target.appendChild(container);

    if (edges == null)
    {
        return 0;
    }

    if (nodes == null)
    {
        return -2;
    }

    target = document.getElementById("edges");

    if (target == null)
    {
        throw "Renderer: Can't find target by ID 'node'.";
    }

    let connections = new Map();

    for (let edge of edges)
    {
        if (edge.getFrom() == currentNode.getId())
        {
            if (connections.has(edge.getFrom()) == false)
            {
                connections.set(edge.getFrom(), new Array());
            }

            connections.get(edge.getFrom()).push(edge.getTo());
        }
        else if (edge.getTo() == currentNode.getId())
        {
            if (connections.has(edge.getFrom()) == false)
            {
                connections.set(edge.getFrom(), new Array());
            }

            connections.get(edge.getFrom()).push(edge.getTo());
        }
    }

    for (let connection of connections)
    {
        if (connection[0] == currentNode.getId())
        {
            // connection[0] is probably key, connection[1] probably value.
            for (let i = 0, max = connection[1].length; i &lt; max; i++)
            {
                if (nodes.has(connection[1][i]) == false)
                {
                    throw "Reference '" + connection[1][i] + "' in edges is missing in nodes.";
                }

                container = document.createElement("div");

                if (connections.has(connection[1][i]) == true)
                {
                    // Should be only one with reference back to connection/currentNode.
                    for (let j = 0, max = connections.get(connection[1][i]).length; j &lt; max; j++)
                    {
                        if (connections.get(connection[1][i])[j] == connection[0])
                        {
                            let arrow = document.createTextNode("🠘");
                            container.appendChild(arrow);

                            break;
                        }
                    }
                }

                let arrow = document.createTextNode("🠚");
                container.appendChild(arrow);

                container.appendChild(document.createTextNode(" "));

                let navigationLink = document.createElement("a");
                // TODO: Prevent injection (any maybe at other places, too).
                navigationLink.setAttribute("href", connection[1][i] + ".xhtml");

                let title = document.createTextNode(nodes.get(connection[1][i]).getTitle());
                navigationLink.appendChild(title);

                container.appendChild(navigationLink);

                target.appendChild(container);
            }
        }
        else
        {
            // connection[0] is probably key, connection[1] probably value.
            for (let i = 0, max = connection[1].length; i &lt; max; i++)
            {
                if (connection[1][i] != currentNode.getId())
                {
                    throw "Edge reference not targeting currentNode.";
                }

                // Should be present and currentNode, only one loop iteration anyway
                // (no connections represented between the the edge nodes).

                if (nodes.has(connection[1][i]) == false)
                {
                    throw "Reference '" + connection[1][i] + "' in edges is missing in nodes.";
                }

                // Certainly is always true.
                if (connections.has(connection[1][i]) == true)
                {
                    // This lookup again is kind of stupid, could be filtered more smartly/elegantly.

                    let found = false;

                    for (let j = 0, max = connections.get(connection[1][i]).length; j &lt; max; j++)
                    {
                        if (connections.get(connection[1][i])[j] == connection[0])
                        {
                            // Should already have been handled by the loop branch
                            // doing connection[0] == currentNode.getId() above.

                            found = true;
                            break;
                        }
                    }

                    if (found == true)
                    {
                        continue;
                    }
                }

                container = document.createElement("div");

                let arrow = document.createTextNode("🠘");
                container.appendChild(arrow);

                container.appendChild(document.createTextNode(" "));

                // Toggle this for uni-directionality.
                if (true)
                {
                    let navigationLink = document.createElement("a");
                    // TODO: Prevent injection (any maybe at other places, too).
                    navigationLink.setAttribute("href", connection[0] + ".xhtml");

                    let title = document.createTextNode(nodes.get(connection[0]).getTitle());
                    navigationLink.appendChild(title);

                    container.appendChild(navigationLink);
                }
                else
                {
                    let title = document.createTextNode(nodes.get(connection[0]).getTitle());
                    container.appendChild(title);
                }

                target.appendChild(container);
            }
        }
    }

    return true;
}


// -- Navigation --

let graphLoader = new GraphLoader();

function NavigateNode()
{
    let currentNode = graphLoader.getCurrentNode();
    let nodes = graphLoader.getNodes();
    let edges = graphLoader.getEdges();

    if (currentNode != null)
    {
        RenderNode(currentNode, nodes, edges);
    }
    else
    {
        console.log("No current node!");
    }
}


// -- local --

function loadGraph() {
    NavigateNode();
}

window.onload = function () {
    let loadLink = document.getElementById('loadLink');
    loadLink.parentNode.removeChild(loadLink);

    loadGraph();
};
</xsl:text>
        </script>
      </head>
      <body>
        <div id="graph">
          <div id="node"/>
          <div id="edges"/>
          <div id="loadLink">
            <a href="#" onclick="loadGraph();">Load</a>
          </div>
        </div>
        <div id="graphml-input" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/graphml:graphml"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//graphml:*">
    <xsl:element name="graphml:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//graphml:*//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
