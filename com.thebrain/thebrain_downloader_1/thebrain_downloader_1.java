/* Copyright (C) 2021  Stephan Kreutzer
 *
 * This file is part of thebrain_downloader_1.
 *
 * thebrain_downloader_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * thebrain_downloader_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with thebrain_downloader_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2021-09-25
 */



import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLStreamException;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;



public class thebrain_downloader_1
{
    public static void main(String[] args)
    {
        System.out.print("thebrain_downloader_1 Copyright (C) 2021 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/clients/ and the project website\n" +
                         "https://www.publishing-systems.org.\n\n");

        thebrain_downloader_1 instance = new thebrain_downloader_1();
        instance.run(args);
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("Usage:\n\tthebrain_downloader_1 brain-id [thought-id] target-directory\n");
            System.exit(1);
        }

        this.programPath = thebrain_downloader_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            this.programPath = new File(this.programPath).getCanonicalPath() + File.separator;
            this.programPath = URLDecoder.decode(this.programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("thebrain_downloader_1: Can't determine program path.");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: Can't determine program path.");
            System.exit(1);
        }

        String apiUrl = "https://api.thebrain.com/api-v11/";
        String brainId = args[0];


        File tempDirectory = new File(this.programPath + "temp");

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    System.out.println("thebrain_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("thebrain_downloader_1: Can't create temp directory \"" + tempDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        String outputParentPath = null;

        if (args.length == 2)
        {
            outputParentPath = args[1];
        }
        else if (args.length >= 3)
        {
            outputParentPath = args[2];
        }

        if (outputParentPath.endsWith("/") != true &&
            outputParentPath.endsWith("\\") != true)
        {
            outputParentPath += File.separator;
        }

        File outputDirectory = new File(outputParentPath + "thoughts");

        try
        {
            outputDirectory = outputDirectory.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("thebrain_downloader_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: Can't get canonical path of output directory \"" + outputDirectory.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (outputDirectory.exists() == true)
        {
            if (outputDirectory.isDirectory() == true)
            {
                System.out.println("thebrain_downloader_1: Output directory \"" + outputDirectory.getAbsolutePath() + "\" does already exist.");
                System.exit(1);
            }
            else
            {
                System.out.println("thebrain_downloader_1: Output path \"" + outputDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                outputDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("thebrain_downloader_1: Can't create output directory \"" + outputDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        File logDirectory = new File(tempDirectory.getAbsolutePath() + File.separator + "log");

        if (logDirectory.exists() == true)
        {
            if (logDirectory.isDirectory() == true)
            {
                System.out.println("thebrain_downloader_1: Log directory \"" + logDirectory.getAbsolutePath() + "\" does already exist.");
                System.exit(1);
            }
            else
            {
                System.out.println("thebrain_downloader_1: Log path \"" + logDirectory.getAbsolutePath() + "\" isn't a directory.");
                System.exit(1);
            }
        }
        else
        {
            try
            {
                logDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                System.out.println("thebrain_downloader_1: Can't create log directory \"" + logDirectory.getAbsolutePath() + "\".");
                System.exit(1);
            }
        }

        String thoughtId = null;

        if (args.length >= 3)
        {
            thoughtId = args[1];
        }
        else
        {
            attemptDownload(apiUrl + "brains/" + brainId, "start", tempDirectory, tempDirectory);

            {
                File startFile = new File(tempDirectory.getAbsolutePath() + File.separator + "start.xml");

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(startFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String tagName = event.asStartElement().getName().getLocalPart();

                            if (tagName.equals("home-thought-id") == true)
                            {
                                StringBuilder sb = new StringBuilder();

                                while (eventReader.hasNext() == true)
                                {
                                    event = eventReader.nextEvent();

                                    if (event.isCharacters() == true)
                                    {
                                        sb.append(event.asCharacters());
                                    }
                                    else if (event.isEndElement() == true)
                                    {
                                        tagName = event.asEndElement().getName().getLocalPart();

                                        if (tagName.equals("home-thought-id") == true)
                                        {
                                            thoughtId = sb.toString();
                                            break;
                                        }
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + startFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (SecurityException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + startFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (IOException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + startFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
            }

            if (thoughtId == null)
            {
                System.out.println("thebrain_downloader_1: Failed to determine thought-id.");
                System.exit(1);
            }
        }

        /** @todo Check thoughtId (path) */


        Set<String> thoughts = new HashSet<String>();
        thoughts.add(thoughtId);

        for (long thoughtIndex = 1L; true; thoughtIndex++)
        {
            System.out.println("[size]: " + thoughts.size() + "\n");

            {
                Iterator<String> iter = thoughts.iterator();

                if (iter.hasNext() == true)
                {
                    thoughtId = iter.next();
                }
                else
                {
                    break;
                }
            }

            {
                File logFile = new File(logDirectory.getAbsolutePath() + File.separator + thoughtIndex + ".log");

                if (logFile.exists() == true)
                {
                    System.out.println("thebrain_downloader_1: Log file \"" + logFile.getAbsolutePath() + "\" exists already.");
                    System.exit(1);
                }

                try
                {
                    BufferedWriter writer = new BufferedWriter(
                                            new OutputStreamWriter(
                                            new FileOutputStream(logFile),
                                            "UTF-8"));

                    Iterator<String> iter = thoughts.iterator();

                    while (iter.hasNext() == true)
                    {
                        writer.write(iter.next());
                        writer.write("\n");
                    }

                    writer.flush();
                    writer.close();
                }
                catch (FileNotFoundException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while writing log file \"" + logFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (UnsupportedEncodingException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while writing log file \"" + logFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (IOException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while writing log file \"" + logFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
            }

            attemptDownload(apiUrl + "brains/" + brainId + "/thoughts/" + thoughtId + "/graph", thoughtId, outputDirectory, tempDirectory);

            {
                File thoughtFile = new File(outputDirectory.getAbsolutePath() + File.separator + thoughtId + ".xml");

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(thoughtFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String tagName = event.asStartElement().getName().getLocalPart();

                            if (tagName.equals("parents") == true ||
                                tagName.equals("jumps") == true ||
                                tagName.equals("children") == true ||
                                tagName.equals("siblings") == true)
                            {
                                while (eventReader.hasNext() == true)
                                {
                                    event = eventReader.nextEvent();

                                    if (event.isStartElement() == true)
                                    {
                                        if (event.asStartElement().getName().getLocalPart().equals("string") == true)
                                        {
                                            StringBuilder sb = new StringBuilder();

                                            while (eventReader.hasNext() == true)
                                            {
                                                event = eventReader.nextEvent();

                                                if (event.isCharacters() == true)
                                                {
                                                    sb.append(event.asCharacters());
                                                }
                                                else if (event.isEndElement() == true)
                                                {
                                                    if (event.asEndElement().getName().getLocalPart().equals("string") == true)
                                                    {
                                                        System.out.print(tagName + ": " + sb.toString());

                                                        File testFile = new File(outputDirectory.getAbsolutePath() + File.separator + sb.toString() + ".xml");

                                                        if (testFile.exists() != true)
                                                        {
                                                            if (thoughts.add(sb.toString()) != true)
                                                            {
                                                                System.out.print(" -> known");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            System.out.print(" -> present");
                                                        }

                                                        System.out.print("\n");

                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (event.isEndElement() == true)
                                    {
                                        if (event.asEndElement().getName().getLocalPart().equals(tagName) == true)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + thoughtFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (SecurityException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + thoughtFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
                catch (IOException ex)
                {
                    System.out.println("thebrain_downloader_1: An error occurred while reading \"" + thoughtFile.getAbsolutePath() + "\".");
                    System.exit(1);
                }
            }

            thoughts.remove(thoughtId);

        }

        return 0;
    }

    protected int attemptDownload(String url, String name, File outputDirectory, File tempDirectory)
    {
        /** @todo Check status 200. */
        attemptRetrieval(url, name, tempDirectory, outputDirectory);

        File jsonFile = new File(outputDirectory.getAbsolutePath() + File.separator + name + ".json");
        File xmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + name + ".xml");

        jsonToXml(jsonFile, tempDirectory, xmlFile);

        return 0;
    }

    public int attemptRetrieval(String url, String name, File tempDirectory, File targetDirectory)
    {
        if (tempDirectory.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (tempDirectory.isDirectory() != true)
        {
            System.out.println("thebrain_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (tempDirectory.canWrite() != true)
        {
            System.out.println("thebrain_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        if (targetDirectory.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Target directory \"" + targetDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (targetDirectory.isDirectory() != true)
        {
            System.out.println("thebrain_downloader_1: Target path \"" + targetDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (targetDirectory.canWrite() != true)
        {
            System.out.println("thebrain_downloader_1: Target directory \"" + targetDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        // Ampersand needs to be the first, otherwise it would double-encode
        // other entities.
        url = url.replaceAll("&", "&amp;");
        url = url.replaceAll("<", "&lt;");
        url = url.replaceAll(">", "&gt;");
        url = url.replaceAll("\"", "&quot;");
        url = url.replaceAll("'", "&apos;");

        File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_https_client_1_" + name + ".xml");
        File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_https_client_1_" + name + ".xml");
        File resourceFile = new File(targetDirectory.getAbsolutePath() + File.separator + name + ".json");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resourceFile.exists() == true)
        {
            if (resourceFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resourceFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resourceFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite resource file \"" + resourceFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Resource path \"" + resourceFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by thebrain_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and https://www.publishing-systems.org). -->\n");
            writer.write("<https-client-1-jobfile>\n");
            writer.write("  <request url=\"" + url + "\" method=\"GET\"/>\n");
            writer.write("  <response destination=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
            writer.write("</https-client-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "https_client_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "https_client" + File.separator + "https_client_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading https_client_1 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("thebrain_downloader_1: https_client_1 call wasn't successful.");
            System.exit(1);
        }

        try
        {
            // Be a friendly agent, not a greedy Denial-of-Service attack.
            Thread.sleep(5000);
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
            System.exit(-1);
        }

        return 0;
    }

    public int jsonToXml(File inputFile, File tempDirectory, File outputFile)
    {
        if (inputFile.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.out.println("thebrain_downloader_1: Input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.out.println("thebrain_downloader_1: Input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        if (tempDirectory.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (tempDirectory.isDirectory() != true)
        {
            System.out.println("thebrain_downloader_1: Temp path \"" + tempDirectory.getAbsolutePath() + "\" isn't a directory.");
            System.exit(1);
        }

        if (tempDirectory.canWrite() != true)
        {
            System.out.println("thebrain_downloader_1: Temp directory \"" + tempDirectory.getAbsolutePath() + "\" isn't writable.");
            System.exit(1);
        }

        File jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_json_to_xml_2.xml");
        File resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_json_to_xml_2.xml");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (outputFile.exists() == true)
        {
            if (outputFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = outputFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (outputFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite output file \"" + outputFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Output path \"" + outputFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        File tempJsonxmlFile = new File(tempDirectory.getAbsolutePath() + File.separator + "temp.jsonxml");

        if (tempJsonxmlFile.exists() == true)
        {
            if (tempJsonxmlFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = tempJsonxmlFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (tempJsonxmlFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite temporary output file \"" + tempJsonxmlFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Temporary output path \"" + tempJsonxmlFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by thebrain_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and https://www.publishing-systems.org). -->\n");
            writer.write("<json-to-xml-2-jobfile>\n");
            writer.write("  <json-input-file path=\"" + inputFile.getAbsolutePath() + "\"/>\n");
            writer.write("  <xml-output-file path=\"" + tempJsonxmlFile.getAbsolutePath() + "\"/>\n");
            writer.write("</json-to-xml-2-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        ProcessBuilder builder = new ProcessBuilder("java", "json_to_xml_2", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "json_to_xml" + File.separator + "json_to_xml_2"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading json_to_xml_2 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("thebrain_downloader_1: json_to_xml_2 call wasn't successful.");
            System.exit(1);
        }


        jobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_jsonxml_to_xml_1.xml");
        resultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_jsonxml_to_xml_1.xml");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite jobfile \"" + jobFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Job path \"" + jobFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        System.out.println("thebrain_downloader_1: Can't overwrite result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
                        System.exit(1);
                    }
                }
            }
            else
            {
                System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by thebrain_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and http://www.publishing-systems.org). -->\n");
            writer.write("<jsonxml-to-xml-1-jobfile>\n");
            writer.write("  <jsonxml-input-file path=\"" + tempJsonxmlFile.getAbsolutePath() + "\"/>\n");
            writer.write("  <xml-output-file path=\"" + outputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</jsonxml-to-xml-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while writing jobfile \"" + jobFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        builder = new ProcessBuilder("java", "jsonxml_to_xml_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + "digital_publishing_workflow_tools" + File.separator + "json_to_xml" + File.separator + "json_to_xml_2" + File.separator + "jsonxml_to_xml" + File.separator + "jsonxml_to_xml_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading jsonxml_to_xml_1 output.");
            System.exit(1);
        }

        if (resultInfoFile.exists() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" doesn't exist, but should.");
            System.exit(1);
        }

        if (resultInfoFile.isFile() != true)
        {
            System.out.println("thebrain_downloader_1: Result information path \"" + resultInfoFile.getAbsolutePath() + "\" exists, but isn't a file.");
            System.exit(1);
        }

        if (resultInfoFile.canRead() != true)
        {
            System.out.println("thebrain_downloader_1: Result information file \"" + resultInfoFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (SecurityException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("thebrain_downloader_1: An error occurred while reading result information file \"" + resultInfoFile.getAbsolutePath() + "\".");
            System.exit(1);
        }

        if (wasSuccess != true)
        {
            System.out.println("thebrain_downloader_1: jsonxml_to_xml_1 call wasn't successful.");
            System.exit(1);
        }

        return 0;
    }

    protected String programPath = null;
}
