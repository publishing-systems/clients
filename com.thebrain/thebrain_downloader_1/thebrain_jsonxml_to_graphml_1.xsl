<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2021 Stephan Kreutzer

This file is part of thebrain_downloader_1.

thebrain_downloader_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

thebrain_downloader_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with thebrain_downloader_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:json="htx-scheme-id://org.json.20000508T164228Z/json/ecma-404/v2" exclude-result-prefixes="json">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="/">
    <graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
      <xsl:comment> This file was created by thebrain_jsonxml_to_graphml_1.xsl of thebrain_downloader_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and https://publishing-systems.org). </xsl:comment>
      <key id="title" for="node" attr.name="label" attr.type="string"/>
      <key id="text" for="node" attr.name="description" attr.type="string"/>
      <graph id="TODO" edgedefault="directed">
        <xsl:apply-templates select="./json:object/thoughts/json:object[./id/text()=/json:object/root/id/text()]"/>
        <xsl:apply-templates select="./json:object/thoughts/json:object[not(./id/text()=/json:object/root/id/text())]"/>
        <xsl:apply-templates select="./json:object/root/parents/json:string"/>
        <xsl:apply-templates select="./json:object/root/children/json:string"/>
        <xsl:apply-templates select="./json:object/root/jumps/json:string"/>
      </graph>
    </graphml>
  </xsl:template>

  <xsl:template match="/json:object/thoughts/json:object">
    <xsl:if test="not(./id/text()=/json:object/root/siblings/json:string/text())">
      <node id="{./id/text()}">
        <!--
          GraphML doesn't support semantics for the data of a node, just this generic mechanism. Writing
          our own schema as suggested in http://graphml.graphdrawing.org/primer/graphml-primer.html#EXT
          isn't cool and the official standard lacks support for a complex type with lax validation.
        -->
        <data key="title"><xsl:value-of select="./name/text()"/></data>
        <xsl:if test="./id/text()=/json:object/root/id/text()">
          <xsl:apply-templates select="/json:object/root/attachments/json:string"/>
        </xsl:if>
      </node>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/json:object/root/parents/json:string">
    <edge id="edge-parent-{position()}" source="{./text()}" target="{/json:object/root/id/text()}"/>
  </xsl:template>

  <xsl:template match="/json:object/root/children/json:string">
    <edge id="edge-child-{position()}" source="{/json:object/root/id/text()}" target="{./text()}"/>
  </xsl:template>

  <xsl:template match="/json:object/root/jumps/json:string">
    <edge id="edge-jump-{position()}-1" source="{/json:object/root/id/text()}" target="{./text()}"/>
    <edge id="edge-jump-{position()}-2" source="{./text()}" target="{/json:object/root/id/text()}"/>
  </xsl:template>

  <xsl:template match="/json:object/root/attachments/json:string">
    <xsl:apply-templates select="/json:object/attachments/json:object[./id/text() = current()/text()]"/>
  </xsl:template>

  <xsl:template match="/json:object/attachments/json:object">
    <xsl:choose>
      <xsl:when test="./type/text() = '1'">
        <data key="text">
          <xsl:apply-templates select="/json:object/notesHtml/* | /json:object/notesHtml//text()"/>
        </data>
      </xsl:when>
      <xsl:when test="./type/text() = '3'">
        <data key="text">
          <!-- Output this in escaped form, because /json:object/notesHtml is escaped in the source as well. -->
          <xsl:text>&lt;a href="</xsl:text>
          <xsl:value-of select="./location/text()"/>
          <xsl:text>"&gt;</xsl:text>
          <xsl:value-of select="./name/text()"/>
          <xsl:text>&lt;/a&gt;</xsl:text>
        </data>
      </xsl:when>
      <xsl:when test="./type/text() = '5'">
        <!-- No support for icon. -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:comment>
          <xsl:text> Attachment type </xsl:text>
          <xsl:value-of select="./type/text()"/>
          <xsl:text> is not supported. </xsl:text>
        </xsl:comment>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/json:object/notesHtml/*">
    <xsl:element name="html:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="/json:object/notesHtml//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
