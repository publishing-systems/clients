#!/bin/sh
# Copyright (C) 2019-2021 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./temp/
mkdir -p ./result/graphml/

printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./temp/jobfile_thebrain_jsonxml_to_graphml.xml
printf "<xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_thebrain_jsonxml_to_graphml.xml

for filepath in ./result/thoughts/*.xml
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "  <job input-file=\"../result/thoughts/%s.xml\" entities-resolver-config-file=\"../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_empty.xml\" stylesheet-file=\"../thebrain_jsonxml_to_graphml_1.xsl\" output-file=\"../result/graphml/%s.graphml\" input-file-equals-output-file-overwrite=\"false\"/>\n" $filename $filename >> ./temp/jobfile_thebrain_jsonxml_to_graphml.xml
done

printf "</xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_thebrain_jsonxml_to_graphml.xml

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./temp/jobfile_thebrain_jsonxml_to_graphml.xml ./temp/resultinfo_thebrain_jsonxml_to_graphml.xml


mkdir -p ./result/graph/

printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./temp/jobfile_graphml_to_graph.xml
printf "<xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_graphml_to_graph.xml

for filepath in ./result/graphml/*.graphml
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "  <job input-file=\"../result/graphml/%s.graphml\" entities-resolver-config-file=\"../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_empty.xml\" stylesheet-file=\"../graphml_to_graph_1.xsl\" output-file=\"../result/graph/%s.xhtml\" input-file-equals-output-file-overwrite=\"false\"/>\n" $filename $filename >> ./temp/jobfile_graphml_to_graph.xml
done

printf "</xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_graphml_to_graph.xml

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./temp/jobfile_graphml_to_graph.xml ./temp/resultinfo_thebrain_graphml_to_graph.xml
