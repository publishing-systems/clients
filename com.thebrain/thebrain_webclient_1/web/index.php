<?php
/* Copyright (C) 2019-2021  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/index.php
 * @todo This still has the old interface version with the "Details" tab blocking links from
 *     being clickable. Prior to serious use, port-over/integrate the improved, fixed newer version.
 * @author Stephan Kreutzer
 * @since 2019-10-12
 */



if (isset($_GET["brain-id"]) !== true)
{
    http_response_code(400);
    exit(0);
}

if (isset($_GET["thought-id"]) !== true)
{
    http_response_code(400);
    exit(0);
}

$json = @file_get_contents("https://api.thebrain.com/api-v11/brains/".$_GET["brain-id"]."/thoughts/".$_GET["thought-id"]."/graph");

if ($json === false)
{
    http_response_code(404);
    exit(0);
}

header("Content-Type: application/xhtml+xml");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:sos=\"http://www.untiednations.com/SOS\" xmlns=\"http://www.w3.org/1999/xhtml\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <!--\n".
     "    Copyright (C) 2019-2021 Stephan Kreutzer\n".
     "\n".
     "    This file is part of SOS Server Prototype 2.\n".
     "\n".
     "    SOS Server Prototype 2 is free software: you can redistribute it and/or modify\n".
     "    it under the terms of the GNU Affero General Public License version 3 or any later version,\n".
     "    as published by the Free Software Foundation.\n".
     "\n".
     "    SOS Server Prototype 2 is distributed in the hope that it will be useful,\n".
     "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n".
     "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n".
     "    GNU Affero General Public License 3 for more details.\n".
     "\n".
     "    You should have received a copy of the GNU Affero General Public License 3\n".
     "    along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.\n".
     "\n".
     "    The data in the <div id=\"sos-input\"/> is not part of this program,\n".
     "    it's user data that is only processed. A different license may apply.\n".
     "    -->\n".
     "    <title>TheBrain Grid</title>\n".
     "    <meta content=\"initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\" name=\"viewport\"/>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/grid_styles.css\"/>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-engine.js\">//</script>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-renderer.js\">//</script>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-navigation.js\">//</script>\n".
     "    <script type=\"text/javascript\">\n".
     "\"use strict\";\n".
     "\n".
     "// As browsers/W3C/WHATWG turned incredibly evil, they might ignore the self-declarative\n".
     "// XML namespace of this document and the given content type in the header, and instead\n".
     "// assume/render \"text/html\", which then fails with JavaScript characters that are\n".
     "// XML/XHTML special characters which need to be escaped, if the file is saved under\n".
     "// a name that happens to end with \".html\" (!!!). Just have the file name end with\n".
     "// \".xhtml\" and it might magically start to work.\n".
     "\n".
     "function loadGrid()\n".
     "{\n".
     "    NavigatePoi(".htmlspecialchars(json_encode($_GET['brain-id']), ENT_XHTML, "UTF-8").", ".htmlspecialchars(json_encode($_GET['thought-id']), ENT_XHTML, "UTF-8").");\n".
     "}\n".
     "\n".
     "window.onload = function () {\n".
     "    let loadLink = document.getElementById('loadLink');\n".
     "    loadLink.parentNode.removeChild(loadLink);\n".
     "\n".
     "    loadGrid();\n".
     "};\n".
     "    </script>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div id=\"grid\">\n".
     "      <div id=\"causes\"/>\n".
     "      <div id=\"current\"/>\n".
     "      <div id=\"effects\"/>\n".
     "      <div id=\"details\"/>\n".
     "      <div id=\"loadLink\">\n".
     "        <a href=\"#\" onclick=\"loadGrid();\">Load</a>\n".
     "      </div>\n".
     "    </div>\n".
     "    <div id=\"sos-input\" style=\"display:none;\">\n".
     "      <!-- The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. -->\n".
     "      <sos:SOS Language=\"en\" xsi:schemaLocation=\"http://www.untiednations.com/SOS http://www.untiednations.com/XML/SOSMeeting2.xsd\">\n";

$json = json_decode($json, true);

$parents = array();
$jumps = array();
$children = array();
$nodes = array();
$attachments = array();

$nodes[$_GET['thought-id']] = array();

if (isset($json["root"]["parents"]) === true)
{
    for ($i = 0, $max = count($json["root"]["parents"]); $i < $max; $i++)
    {
        $parents[] = $json["root"]["parents"][$i];
        $nodes[$json["root"]["parents"][$i]] = array();
    }
}

if (isset($json["root"]["jumps"]) === true)
{
    for ($i = 0, $max = count($json["root"]["jumps"]); $i < $max; $i++)
    {
        $jumps[] = $json["root"]["jumps"][$i];
        $nodes[$json["root"]["jumps"][$i]] = array();
    }
}

if (isset($json["root"]["children"]) === true)
{
    for ($i = 0, $max = count($json["root"]["children"]); $i < $max; $i++)
    {
        $children[] = $json["root"]["children"][$i];
        $nodes[$json["root"]["children"][$i]] = array();
    }
}

if (count($nodes) > 0)
{
    $max = count($json["thoughts"]);

    foreach ($nodes as $id => &$node)
    {
        for ($i = 0; $i < $max; $i++)
        {
            if ($json["thoughts"][$i]["id"] === $id)
            {
                $node = array();
                $node["name"] = $json["thoughts"][$i]["name"];
                break;
            }
        }

        if (count($node) <= 0)
        {
            
        }
    }
}

if (isset($json["root"]["attachments"]) === true)
{
    for ($i = 0, $max = count($json["root"]["attachments"]); $i < $max; $i++)
    {
        $found = false;

        for ($j = 0, $max = count($json["attachments"]); $j < $max; $j++)
        {
            if ($json["attachments"][$j]["id"] == $json["root"]["attachments"][$i])
            {
                $attachments[] = $json["attachments"][$j];

                $found = true;
                break;
            }
        }

        if ($found == false)
        {
            
        }
    }
}

echo "        <sos:POI ID=\"".htmlspecialchars($_GET['thought-id'], ENT_XHTML, "UTF-8")."\">\n".
     "          <sos:Title>".htmlspecialchars($nodes[$_GET['thought-id']]["name"], ENT_XHTML, "UTF-8")."</sos:Title>\n";

if (count($jumps) > 0)
{
    echo "          <!-- Jumps are not part of SOS format! Is just a quick lazy hack. -->\n".
         "          <sos:Jumps>";

    for ($i = 0, $max = count($jumps); $i < $max; $i++)
    {
        echo "<hr/>";

        echo "<a href=\"index.php?brain-id=".htmlspecialchars($_GET["brain-id"], ENT_XHTML, "UTF-8")."&amp;thought-id=".htmlspecialchars($jumps[$i], ENT_XHTML, "UTF-8")."\">".htmlspecialchars($nodes[$jumps[$i]]["name"], ENT_XHTML, "UTF-8")."</a>";
    }

    echo "</sos:Jumps>\n";
}

for ($i = 0, $max = count($parents); $i < $max; $i++)
{
    echo "          <sos:Parent Parent-ID=\"".htmlspecialchars($parents[$i], ENT_XHTML, "UTF-8")."\"/>\n";
}

if (count($attachments) > 0)
{
    $text = "";

    foreach ($attachments as $attachment)
    {
        if ($attachment["type"] == 1)
        {
            // HTML intentionally left escaped for now.
            $text = $json["notesHtml"];
        }
        else if ($attachment["type"] == 3)
        {
            // Name: $attachment["name"]
            /** @todo Escaping? */
            $text = "<a href=\"".$attachment["location"]."\">".$attachment["name"]."</a>";
        }
        else if ($attachment["type"] == 5)
        {
            // No support for icon.
        }
        else
        {
            
        }
    }

    echo "          <sos:Text>".htmlspecialchars($text, ENT_XHTML, "UTF-8")."</sos:Text>\n";
}

echo "        </sos:POI>\n";

for ($i = 0, $max = count($parents); $i < $max; $i++)
{
    echo "        <sos:POI ID=\"".htmlspecialchars($parents[$i], ENT_XHTML, "UTF-8")."\">\n".
         "          <sos:Title>".htmlspecialchars($nodes[$parents[$i]]["name"], ENT_XHTML, "UTF-8")."</sos:Title>\n".
         "        </sos:POI>\n";
}

for ($i = 0, $max = count($children); $i < $max; $i++)
{
    echo "        <sos:POI ID=\"".htmlspecialchars($children[$i], ENT_XHTML, "UTF-8")."\">\n".
         "          <sos:Title>".htmlspecialchars($nodes[$children[$i]]["name"], ENT_XHTML, "UTF-8")."</sos:Title>\n".
         "          <sos:Parent Parent-ID=\"".htmlspecialchars($_GET['thought-id'], ENT_XHTML, "UTF-8")."\"/>\n".
         "        </sos:POI>\n";
}

echo "      </sos:SOS>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
