/* Copyright (C) 2019-2021 Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function SosGridEngine()
{
    let that = this;

    // Pass the ID of the POI, which is a string. Empty string to get the first,
    // default POI. Returns null if POI can't be obtained.
    // TODO: When requesting the default, the actual ID of the first POI should
    // be in the returned Poi object.
    that.getPoiById = function(id) {
        if (typeof id !== 'string' && !(id instanceof String))
        {
            id = "";
        }

        if (_loaded == false)
        {
            _load();
        }

        if (_loaded == false)
        {
            throw "Loading failed."
        }

        let poiSource = null;

        if ("querySelectorAll" in document)
        {
            if (id.length <= 0)
            {
                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("POI");

                for (let i = 0; i < result.length; i++)
                {
                    if (result[i].namespaceURI == "http://www.untiednations.com/SOS")
                    {
                        poiSource = result[i];
                        break;
                    }
                }
            }
            else
            {
                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("POI[ID='" + id + "']");
                let pois = new Array();

                for (let i = 0; i < result.length; i++)
                {
                    if (result[i].namespaceURI == "http://www.untiednations.com/SOS")
                    {
                        pois.push(result[i]);
                    }
                }

                if (pois.length <= 0)
                {
                    
                }
                else if (pois.length > 1)
                {
                    throw "'ID' attribute value '" + id + "' of a POI found more than once.";
                }

                poiSource = pois[0];
            }
        }
        else
        {
            if (id.length <= 0)
            {
                if (_poiMapping.length > 0)
                {
                    for (let item in _poiMapping)
                    {
                        poiSource = _poiMapping[item];
                        break;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (!(id in _poiMapping))
                {
                    return null;
                }

                poiSource = _poiMapping[id];
            }
        }

        if (poiSource == null)
        {
            return null;
        }

        if (poiSource.hasAttribute("ID") != true)
        {
            throw "POI is missing its 'ID' attribute.";
        }

        let title = null;
        let description = null;
        let jumps = null;
        let parents = new Array();
        let children = new Array();

        for (let i = 0; i < poiSource.children.length; i++)
        {
            let child = poiSource.children[i];

            if (child.namespaceURI != "http://www.untiednations.com/SOS")
            {
                continue;
            }

            if (child.localName == "Title")
            {
                if (title != null)
                {
                    throw "POI contains more than one Title.";
                }

                title = child.textContent;
            }
            else if (child.localName == "Text")
            {
                if (description != null)
                {
                    throw "POI contains more than one Text.";
                }

                description = child.textContent;
            }
            else if (child.localName == "Jumps")
            {
                // This is not part of SOS format/namespace!

                jumps = child.innerHTML;
            }
            else if (child.localName == "Parent")
            {
                if (child.hasAttribute("Parent-ID") != true)
                {
                    throw "Parent of a POI is missing its 'Parent-ID' attribute.";
                }

                let parentId = child.getAttribute("Parent-ID");

                if (parentId.length <= 0)
                {
                    throw "'Parent-ID' attribute value is empty.";
                }

                if (parents.includes(parentId) == true)
                {
                    throw "'Parent-ID' attribute value '" + parentId + "' more than once.";
                }

                if (parentId === id)
                {
                    throw "POI with ID '" + id + "' can't be its own parent.";
                }

                parents.push(parentId);
            }
        }

        if ("querySelectorAll" in document)
        {
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            let result = document.querySelectorAll("Parent[Parent-ID='" + poiSource.getAttribute("ID") + "']");

            for (let i = 0; i < result.length; i++)
            {
                if (result[i].namespaceURI != "http://www.untiednations.com/SOS")
                {
                    continue;
                }

                if (result[i].parentNode.localName != "POI" ||
                    result[i].parentNode.namespaceURI != "http://www.untiednations.com/SOS")
                {
                    throw "Parent of a Parent isn't a proper POI.";
                }

                if (result[i].parentNode.hasAttribute("ID") == true)
                {
                    children.push(result[i].parentNode.getAttribute("ID"));
                }
                else
                {
                    throw "POI is missing its 'ID' attribute.";
                }
            }
        }
        else
        {
            // TODO: The children are not in the source for the current POI and
            // no separate list concludes the children from parent references to
            // add it to all read POIs afterwards, as it needs to be decided if
            // the format should change to support this kind of navigation natively
            // or if it is supposed to be calculated and stored in memory during runtime.
        }

        return new Poi(poiSource.getAttribute("ID"), title, description, jumps, parents, children);
    }

    function _load()
    {
        if (_loaded != false)
        {
            return true;
        }

        if ("querySelectorAll" in document)
        {
            _loaded = true;
            return true;
        }
        else
        {
            console.log("Children of a POI won't be loaded.");
        }

        // Stupid JavaScript/web/browsers: document.getElementsByTagNameNS()
        // seems to not work with XML namespace prefixes.
        let inputXml = document.getElementsByTagName("sos:SOS");

        if (inputXml !== null)
        {
            if (inputXml.length > 0)
            {
                inputXml = inputXml[0].children;

                if (inputXml.length > 0)
                {
                    for (let i = 0; i < inputXml.length; i++)
                    {
                        if (inputXml[i].tagName != "sos:POI")
                        {
                            console.log("Embedded SOS data: Tag '" + inputXml[i].tagName + "' ignored.");
                            continue;
                        }

                        if (inputXml[i].hasAttribute("ID") != true)
                        {
                            throw "Embedded SOS data: Tag '" + inputXml[i].tagName + "' is missing its 'ID' attribute.";
                        }

                        let id = inputXml[i].getAttribute("ID");

                        if (id.length <= 0)
                        {
                            throw "Embedded SOS data: Empty 'ID' attribute of '" + inputXml[i].tagName + "'.";
                        }

                        if (id in _poiMapping)
                        {
                            throw "'ID' attribute value '" + id + "' of a '" + inputXml[i].tagName + "' found more than once.";
                        }

                        // Note: In contrast to querySelectorAll(), getElementsByTagName() returns a *live*
                        // HTMLCollection, so changes to the source affect _poiMapping.
                        _poiMapping[id] = inputXml[i];
                    }

                    _loaded = true;
                }
                else
                {
                    console.log("Embedded SOS data: Is empty.");
                }
            }
        }
    }

    let _loaded = false;
    let _poiMapping = new Array();
}

function Poi(id, title, description, jumps, parents, children)
{
    // TODO: Properly check if parents and chilren are Arrays, and throw exception if not.

    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getTitle = function()
    {
        return _title;
    }

    that.getText = function()
    {
        return _description;
    }

    that.getJumps = function()
    {
        return _jumps;
    }

    that.getParents = function()
    {
        return _parents;
    }

    // TODO: This might not get filled in some cases.
    that.getChildren = function()
    {
        return _children;
    }

    let _id = id;
    let _title = title;
    let _description = description;
    let _jumps = jumps;
    // IDs (strings).
    let _parents = parents;
    // IDs (strings).
    let _children = children;
}
