/* Copyright (C) 2019-2021 Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let currentPoi = null;
let gridEngine = new SosGridEngine();

// TODO: This should be optimized by caching parent and child nodes that
// were already loaded/interpreted for the current POI as its causes and
// effects.

function NavigatePoi(brainId, poiId)
{
    currentPoi = null;

    ResetDetails();
    Reset();

    let poi = gridEngine.getPoiById(poiId);

    if (poi == null)
    {
        throw "Loading a POI failed.";
    }

    if (RenderPoi(brainId, poi) == true)
    {
        currentPoi = poiId;
    }
    else
    {
        return false;
    }

    let parents = new Array();

    for (let i = 0; i < poi.getParents().length; i++)
    {
        parents.push(gridEngine.getPoiById(poi.getParents()[i]));
    }

    if (RenderCauses(brainId, parents) != true)
    {
        return false;
    }

    let children = new Array();

    for (let i = 0; i < poi.getChildren().length; i++)
    {
        children.push(gridEngine.getPoiById(poi.getChildren()[i]));
    }

    if (RenderEffects(brainId, children) != true)
    {
        return false;
    }

    return true;
}

function ShowDetails(poiId)
{
    ResetDetails();

    let poi = gridEngine.getPoiById(poiId);

    if (poi == null)
    {
        throw "Loading a POI failed.";
    }

    if (RenderDetails(poi) != 0)
    {
        return -1;
    }

    return 0;
}
