/* Copyright (C) 2019-2021 Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function RenderPoi(brainId, poi)
{
    return RenderPois(brainId, new Array(poi), "current", "Thought");
}

function RenderCauses(brainId, pois)
{
    // TODO: Properly check if pois is an array.
    return RenderPois(brainId, pois, "causes", "Parents");
}

function RenderEffects(brainId, pois)
{
    // TODO: Properly check if pois is an array.
    return RenderPois(brainId, pois, "effects", "Children");
}

function RenderPois(brainId, pois, targetId, caption)
{
    // TODO: Properly check if pois is an array.

    let target = document.getElementById(targetId);

    if (target == null)
    {
        throw "Renderer: Can't find target with ID '" + targetId + "'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode(caption));
    container.appendChild(header);

    for (let i = 0; i < pois.length; i++)
    {
        if (targetId === "causes")
        {
            let title = document.createTextNode(pois[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + pois[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);
            container.appendChild(document.createTextNode(" "));

            let navigationLink = document.createElement("a");
            // TODO: Prevent injection (and maybe at other places, too).
            navigationLink.setAttribute("href", "grid.php?brain-id=" + brainId + "&thought-id=" + pois[i].getId());
            //navigationLink.setAttribute("onclick", "NavigatePoi(\"" + pois[i].getId() + "\");");

            let arrow = document.createTextNode("➔");
            navigationLink.appendChild(arrow);
            container.appendChild(navigationLink);
        }
        else if (targetId === "current")
        {
            let title = document.createTextNode(pois[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + pois[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);

            let jumps = document.createElement("div");
            jumps.setAttribute("class", "jumps");
            // TODO: Prevent injection (and maybe at other places, too).
            jumps.innerHTML = pois[i].getJumps();

            container.appendChild(jumps);
        }
        else if (targetId === "effects")
        {
            let navigationLink = document.createElement("a");
            // TODO: Prevent injection (and maybe at other places, too).
            navigationLink.setAttribute("href", "grid.php?brain-id=" + brainId + "&thought-id=" + pois[i].getId());
            //navigationLink.setAttribute("onclick", "NavigatePoi(\"" + pois[i].getId() + "\");");

            let arrow = document.createTextNode("➔");
            navigationLink.appendChild(arrow);
            container.appendChild(navigationLink);

            container.appendChild(document.createTextNode(" "));

            let title = document.createTextNode(pois[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + pois[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);
        }
        else
        {
            throw "Renderer: Unknown target ID '" + targetId + "'.";
        }

        /* if (i < pois.length - 1) */
        {
            container.appendChild(document.createElement("hr"));
        }

        if (targetId == "current")
        {
            let source = document.createElement("div");
            source.setAttribute("class", "source");
            // TODO: Prevent injection (and maybe at other places, too).
            source.innerHTML = "[<a href=\"https://app.thebrain.com/brains/" + brainId + "/thoughts/" + pois[i].getId() + "/\">Source</a>]\n";

            container.appendChild(source);
        }
    }

    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("causes", "current", "effects")

    for (let i = 0; i < targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }
}

function RenderDetails(poi)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = poi.getText();

    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }
}
